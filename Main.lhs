%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\documentclass[a4paper,10pt]{report}
%include lhs2TeX.fmt
%include lhs2TeX.sty

\usepackage{natbib}

\usepackage {mathpartir}      %inference rules
\usepackage {array}

\usepackage{makeidx} 
\makeindex


%to render:
% lhs2TeX --poly Main.lhs > psearch.tex
% latex psearch.tex
% makeindex psearch
% dvips psearch.dvi
% ps2pdf psearch.ps


%some formattings:
%format ~> = "\leadsto "
%format `intersectPT` = "\Cap"
%format toCommonNotation x = "\overline{" x "}^\mathrm{Exp}"
%format toFunctorArguments x = "\overline{" x "}^\mathrm{FAE}"
%format :->: = "\longrightarrow"
%format All x y = "\forall"x"\ "y
%format Pred x y = x"\langle"y"\rangle"
%format (Pred x y) = x"\langle"y"\rangle"
%format :- = "\Longrightarrow"
%subst string a  = "\mathtt{" a "}"

%format flex q = q "_\exists"
%format restrict q c = c "\restriction" q

%format freeVars = "\mathsf{FV}"
%format boundVars = "\mathsf{BV}"
%format compact x = x"\dagger"
%format compact1 x = x"\ddagger"
%format normalise x = x"\Downarrow"

%format map f l = f "\;\overrightarrow{" l "}"
%format concatMap f l = "\bigcup" f "\overrightarrow{" l "}"
%format Pred x (y) = x"\langle"y"\rangle"

%format freeVarsFAE = "\mathsf{FV}_\mathrm{FAE}"
%format boundVarsFAE = "\mathsf{BV}_\mathrm{FAE}"
%format freeVarsExp = "\mathsf{FV}_\mathrm{Exp}"
%format boundVarsExp = "\mathsf{BV}_\mathrm{Exp}"

%format rho = "\rho"
%format rho' = "\rho^\prime"
%format phi = "\phi"
%format phi' = "\phi^\prime"




\newcommand{\ignore}[1]{}
\oddsidemargin 0pc
\evensidemargin 0pc
%\textheight 666pt       % 9 1/4 column height
\textwidth 39pc


\usepackage[ps2pdf,bookmarks=true,plainpages=false,pdfpagelabels]{hyperref}

\hypersetup{
pdfauthor = {Nik Sultana},
pdftitle = {Proof Search for Minimal Logic in Haskell},
pdfsubject = {Mechanised logic},
pdfkeywords = {Minimal logic, Automatic theorem proving, Haskell},
pdfcreator = {LaTeX with hyperref package},
pdfproducer = {dvips + ps2pdf}}




\begin{document}

\title{Proof Search for Minimal Logic in Haskell}
\author{Nik Sultana \\ Mathematical Institute, \\ University of
    Munich, \\ Germany}\date{}

\maketitle

\begin{abstract}
This report describes a Haskell implementation of Schwichtenberg's proof search algorithm.
Implementations of the algorithm's dependencies are also described: Wand's type inference algorithm, Normalisation by Evaluation, and Pattern Unification.
\end{abstract}

\tableofcontents


\chapter{Introduction}

The theorem prover described in this report accepts formulas in a decidable fragment of minimal quantifier logic and, if they are theorems, returns corresponding proof objects as terms in the simply-typed $\lambda$-calculus. It implements the algorithm described by \cite{schwicht-psml} based on a restricted higher-order unification described by \cite{miller-llambda}. This proof search algorithm has been used in the {\sc MinLog}\footnote{\url{http://www.minlog-system.de/}} proof assistant.
The algorithm's definition assumes terms to be in long normal form; normalisation is carried out here through `normalisation by evaluation' \citep{Berger91}. This last algorithm includes type-indexed functions; types are constructed by means of the type reconstruction algorithm described by \cite{wand-tinference}. 
The report's structure mirrors the dependencies of the components making up the proof system.
These have been implemented in Haskell \citep{jones2003hla} using the so-called `Glasgow extensions'.


\ignore{

>module Main where

>import QPrefix
>import Subst
>import Types
>import Terms
>import NbE
>import Expressions
>import QUnification
>import ProverData
>import Prover
>import General
>import Examples


}



\section{Running the proof system}

The prover is intended to be run in Haskell's top-level. The code presented here has been tested using GHCi version 6.4.1.
Unimportant parts of the source code have been omitted from this report, but 
the source file of this report also doubles as the full Haskell source code of the implementation in literate programming style.
Not all the definitions in this report will immediately appear to be Haskell definitions; this is because the source code presented here has been formatted using the lhs2\TeX\footnote{\url{http://people.cs.uu.nl/andres/lhs2tex/}}~tool for improved readability.

\section{Notation}
This report is generated from the source Haskell code but has been carefully formatted to make the notation more widely recognisable.
The symbol $=$ denotes definition, and the equality test is denoted by the $\equiv$ symbol. The arrow $\rightarrow$ is used both in type signatures and in meta (Haskell) level $\lambda$-abstractions.
A possibly-empty list of items $\vec{x}$ is typed $[\tau]$ for some type $\tau$, and symbols $[]$ and infix : denote nil and cons respectively. Variables $v,x,y,z$ range over variables, $r,s,t$ over terms -- or Q-terms, or patterns: the context will render this clear. Variable $Q$ ranges over quantifier prefixes, $n$ over integers, and variables $\rho$ and $\sigma$ range over substitutions.

The distinction between meta and object levels in terms of notation is as follows: $\lambda$ denotes meta-abstraction; object-level abstraction is denoted by $\lambda\!\!\lambda$. Object-level application is denoted by $\bullet$, while at meta-level it is denoted by either \$ or juxtaposition. The symbol $\cdot$ indicates an empty place in a partially-applied function.



%and now the rest of the report:
%include Expressions.lhs
%include Terms.lhs
%include Subst.lhs

\addcontentsline{toc}{section}{Notes}
\section*{Notes}
It would benefit the implementation to have separate optimised normalisation functions for each representation of $\lambda$-terms, or perhaps use a single representation throughout. Against the backdrop of the usual contention between readability and performance, one would expect that this would diminish readability.

Some definitions described in this chapter could be made more generic: for instance, in the representation types described in \S\ref{sec:Representation}, the type of names could be made a parameter rather than fixed as a string. Similarly, the definition of \emph{Subst} could specify its right components to be values of some type \emph{a} in the class \emph{Term}. In addition, one could make \emph{Subst} into an ADT to restrict the formation of substitutions -- to ensure, for example, that the graphs are functoid.


%include Types.lhs

%include NbE.lhs

%include QPrefix.lhs
%include QUnification.lhs

%include ProverData.lhs
%include Prover.lhs

\chapter{Conclusion}

The small bits of machinery described in each chapter were combined to form the theorem prover, of which some example runs are documented in Appendix~\ref{ch:Examples}. The laminar organisation of the implementation emphasises the interaction between components, and also renders them individually reusable. Using Haskell as the implementation language has paid off primarily in the clarity of the implementation. 
This fulfilled the intention to follow Schwichtenberg's specification closely, even in its presentation.

The implementation's efficiency has not yet been guaged, nor has it been proved correct; these are suggested as future work. Transforming between expression representations is sure to hinder performance, so committing to a single representation is suggested as an improvement.
The presentation suffers from the bureaucratic details describing the handling of variables and ensuring the generation of fresh ones. Although of small interest, these details are crucial to ensure correctness; a more elegant solution to this would be desirable.

This is the second implementation of the algorithm described in \cite{schwicht-psml}, as the first implementation was done in Scheme and forms part of Minlog. Minlog's implementation is more stringent since it allows the user to state types for object variables; it associates arities with predicates; and it also checks whether a unification problem is a pattern unification problem, and whether a formula is a goal or a clause formula. These checks formalise the side-conditions on which the algorithm's correctness is predicated on. However, in the event of invalid input it is more likely to get unification failure rather than an unsound result, by the implementation of the unification algorithm.

This work can be extended in the following ways to match the implementation in Minlog. By implementing the checks mentioned above the proof process can reject invalid input early -- this would require modifying the context's definition to associate types with object variables -- or, for valid formulas, switch to using Huet's unification algorithm when it encounters non-patterns. Furthermore, the logic could be extended to support conjunction and the strong existential quantifier, as explained by \cite{schwicht-psml}.

Schwichtenberg suggested modifying the algorithm to produce proofs devoid of loose variables. In Minlog, canonical type inhabitants are identified upon defining a type, and these are referred to in manual proofs -- appearing in the place of flexible variables in automatically-found proofs. 
Other extensions include extending the configurability of the prover -- regarding trace output and setting multiplicity -- and interfacing it with other systems.



\vfill
\addcontentsline{toc}{chapter}{Acknowledgements}
\begin{center}
\textbf{Acknowledgements}
\end{center}
I thank Prof. Schwichtenberg for his guidance during my stay at LMU.
I have also benefited from feedback during two seminars on this subject given last April.
The talk given by Andreas Abel on the use of monads in logic implementations was instructive.
My stay in Munich was made possible through a Marie Curie fellowship under the auspices of the MATHLOGAPS programme, which I gratefully acknowledge.
%\vfill


\appendix
%include Examples.lhs
%include General.lhs


\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{agsm} 
\bibliography{psearch}

\addcontentsline{toc}{chapter}{Index}
\printindex

\end{document}
