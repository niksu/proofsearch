%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\ignore{

>module Terms where

>import Data.List
>import NbE
>import Expressions

}


\section{Class of terms} \label{sec:class:term}

Irrespective of the representation used for terms -- and assuming concrete variable naming -- one expects certain operations to be definable over terms. These operations are collected to define the \emph{class} of terms, and the two representations of terms defined above are made instances of this class. In the chapters that follow additional instances of this class will be defined, such as Q-terms described in Chapter~\ref{sec:QUnification}.

\newpage

\index{\emph{Term}}
\index{\emph{freeVars}}
\index{\emph{boundVars}}
\index{\emph{compact}}
\index{\emph{compact1}}
\index{\emph{normalise}}

>class Term a where
>  freeVars  :: a -> [String]
>  boundVars :: a -> [String]
>  compact   :: a -> a         --read ``compact''
>  compact1  :: a -> a         --read ``compact1''
>  normalise :: a -> a         --read ``normalise''

\vspace*{1mm}

The role of \emph{compact} is to standardise the presentation of terms in representations, such as \emph{FAE}, in which a normal form might be expressed in various ways -- this contrasts with \emph{Exp} values for which compacting behaves like the identity function. Compacting acts on the dominant constructor in an expression, and subexpressions are subsequently handled by \emph{compact1}. The expected behaviour of the other functions is standard.
Definitions can now be made over the whole class due to the interface shared by its members, irrespective of the precise details of how the interface is implemented.

\vspace*{1mm}

>isClosed :: Term a => a -> Bool
>isClosed t = (freeVars t) == []

\index{\emph{isClosed}}


\subsection{Instantiations}

In this section the representations of terms that were defined earlier are made instances of the class of terms. The instantiation of \emph{Exp} is straightforward and will not be given explicitly here. For this representation, \emph{compact} behaves like the identity function, and \emph{normalise} invokes the normalisation-by-evaluation function that will be described in Chapter~\ref{sec:NbE}.

\index{\emph{Term}!\emph{Exp}}


\ignore{

>instance Term Exp where
>  freeVars = nub.freeVarsExp
>  boundVars = nub.boundVarsExp
>  compact  t = t
>  compact1 = compact
>  normalise t = case (normalisNbE t) of
>                  Nothing  -> t
>                  (Just t')-> t'

}


\noindent In the instantiation of \emph{FAE}, \emph{compact} has more interesting behaviour and standardises the presentation of nested abstractions and applications. This is quite different from normalising a term since \emph{compact} acts on a more superficial level. 
Functions defined over this representation of terms may depend on their input terms being in a compact form. 
In this instantiation \emph{normalise} invokes the normalisation function defined for \emph{Exp} terms, then transforms terms between representations by using the homomorphisms defined in the next section. 

\index{\emph{Term}!\emph{FAE}}

>instance Term FAE where
>  freeVars = nub.freeVarsFAE
>  boundVars = nub.boundVarsFAE
>
>  compact (Var x) = App (Var x) []
>  compact t       = compact1 t
>                     
>  compact1 (Var x) = Var x
>  compact1 (Abs x r) = 
>                       case x of
>                        []   -> compact1 r
>                        vs   -> case r of
>                                    Var v    -> Abs vs (Var v)
>                                    Abs v r' -> compact1 (Abs (vs ++ v) r')
>                                    App r' s' -> Abs x ( App (compact1 r') (map (compact1) s'))
>  compact1 (App r s) = 
>                       case s of
>                        []     -> compact1 r
>                        _      -> case r of
>                                   App r' s' -> compact1 (App r' (s'++s) )
>                                   _         -> App (compact1 r) (map (compact1) s)
>
>  normalise t = 
>                let t' = normalisNbE (toCommonNotation t)
>                in case t' of
>                     Nothing  -> t 
>                     Just t'' -> compact (toFunctorArguments t'')


\section{Translation}
The isomorphism between the representations defined for $\lambda$-terms is witnessed by the functions defined next. 

\index{\emph{toFunctorArguments}}
\index{\emph{toCommonNotation}}

%toFunctorArguments :: Exp -> FAE

>toFunctorArguments (Var' x) = (Var x)
>toFunctorArguments (Abs' x r) = Abs [x] (toFunctorArguments r)
>toFunctorArguments (App' r s) = App (toFunctorArguments r) [toFunctorArguments s]

\vspace*{1mm}

\noindent The second function works in the opposite direction.

\vspace*{1mm}

%toCommonNotation :: FAE -> Exp

>toCommonNotation (Var x)   = Var' x
>toCommonNotation (Abs x r) = 
>                             let mkAbs [] r = r
>                                 mkAbs (x:xs) r = Abs' x (mkAbs xs r)
>                             in  mkAbs x (toCommonNotation r)
>toCommonNotation (App r s) = 
>                             let mkApp r [] = r
>                                 mkApp r (s:ss) = mkApp (App' r (toCommonNotation s)) ss
>                             in  mkApp (toCommonNotation r) s
                                   
