%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\ignore{

>module Examples where
>import QPrefix
>import Subst
>import Types
>import Terms
>import NbE
>import Expressions
>import QUnification
>import ProverData
>import General
>import Prover

}

\chapter{Examples} \label{ch:Examples}

The definitions that follow encode some useful abbreviations. Following these are examples of the theorem prover's use.

\section{Definitions}
The symbol $\bot$ is defined to be the predicate reserved to denote logical falsity. The symbols $\exists^\mathrm{cl}$ and \emph{stab} denote the classical existence and stability schemes respectively.

\begin{code}
bottom = Pred "bottom" []
exca xs f = (All xs (f :->: bottom)) :->: bottom

stab pred xs = All xs (((pred xs :->: bottom) :->: bottom) :->: pred xs)
\end{code}

\section{Example 1}

The first example is a minimally-valid classical statement.

\begin{code}
example1 = 
    search empty ((All ["x"] (Pred "Q" [Var "x"]) :->: (exca ["x"] (Pred "Q" [Var "x"]))))
\end{code}

\noindent The output produced by the algorithm follows:

\begin{verbatim}
===Step 0
 Goal:
:-        (all x. Q x) -> (all x. Q x -> bottom) -> bottom
Stripped implication.
===Step 1
 Goal:
u       : (all x. Q x)
:-        (all x. Q x -> bottom) -> bottom
Stripped implication.
===Step 2
 Goal:
u'      : (all x. Q x -> bottom)
u       : (all x. Q x)
:-        bottom
Resolvable with u'
===Step 3
 Goal:
u'      : (all x. Q x -> bottom)
u       : (all x. Q x)
:-        Q X
Resolvable with u
Completed step 3.
Completed step 2.
Completed step 1.
Completed step 0.
====] Proved [===================
Final sequent was:
u'      : (all x. Q x -> bottom)
u       : (all x. Q x)
:-        bottom

Final context was:
Signatures      : (none)
Flexibles       : X''
Forbiddens      : (none)

Proof   : \u\u'((u' X'') (u X''))
\end{verbatim}

\section{Example 2}

Two slightly different formulas are tested in this example: one is not provable while the other is.

\begin{code}

example2a = 
    search empty (f1 :->: f2 :->: (Pred "Q" []))
    where
          f1 = All ["y"] ((All ["z"] (Pred "R" [Var "y",Var "z"])) :->: Pred "Q" [])
          f2 = All ["y1"] (Pred "R" [Var "y1",Var "y1"])


example2b = 
    search empty (f1 :->: f2 :->: (Pred "Q" []))
    where
          f1 = All ["y"] ((All ["z"] (Pred "R" [Var "y",Var "z"])) :->: Pred "Q" [])
          f2 = All ["y1","y2"] (Pred "R" [Var "y1",Var "y2"])

\end{code}


\vspace*{2mm}

\noindent The output from Example 2A follows:

\begin{verbatim}
===Step 0
 Goal:
:-        (all y. (all z. R y z) -> Q) -> (all y1. R y1 y1) -> Q
Stripped implication.
===Step 1
 Goal:
u       : (all y. (all z. R y z) -> Q)
:-        (all y1. R y1 y1) -> Q
Stripped implication.
===Step 2
 Goal:
u'      : (all y1. R y1 y1)
u       : (all y. (all z. R y z) -> Q)
:-        Q
Resolvable with u
===Step 3
 Goal:
u'      : (all y1. R y1 y1)
u       : (all y. (all z. R y z) -> Q)
:-        (all z. R Y z)
Stripped variable(s).
===Step 4
 Goal:
u'      : (all y1. R y1 y1)
u       : (all y. (all z. R y z) -> Q)
:-        R Y z
Failed to resolve.
Failed step 3.
Failed step 2.
Failed step 1.
Failed step 0.
Could not find a proof.
\end{verbatim}

\begin{center}
\begin{picture}(10,1)(0,0)
%\line(1,0){5cm}
\end{picture}
\end{center}

\noindent Example 2B leads to a positive result as shown next:

\begin{verbatim}
===Step 0
 Goal:
:-        (all y. (all z. R y z) -> Q) -> (all y1,y2. R y1 y2) -> Q
Stripped implication.
===Step 1
 Goal:
u       : (all y. (all z. R y z) -> Q)
:-        (all y1,y2. R y1 y2) -> Q
Stripped implication.
===Step 2
 Goal:
u'      : (all y1,y2. R y1 y2)
u       : (all y. (all z. R y z) -> Q)
:-        Q
Resolvable with u
===Step 3
 Goal:
u'      : (all y1,y2. R y1 y2)
u       : (all y. (all z. R y z) -> Q)
:-        (all z. R Y z)
Stripped variable(s).
===Step 4
 Goal:
u'      : (all y1,y2. R y1 y2)
u       : (all y. (all z. R y z) -> Q)
:-        R Y z
Resolvable with u'
Completed step 4.
Completed step 3.
Completed step 2.
Completed step 1.
Completed step 0.
====] Proved [===================
Final sequent was:
u'      : (all y1,y2. R y1 y2)
u       : (all y. (all z. R y z) -> Q)
:-        Q

Final context was:
Signatures      : (none)
Flexibles       : Y'
Forbiddens      : z

Proof   : \u\u'((u Y') \z((u' (\zY' z)) (\zz z)))
\end{verbatim}


\section{Example 3}

The ``drinker's problem'' formula is proved next. This is not minimally-valid hence requires use of the stability axiom.
Informally the formula expresses that ``there is a person such that when that person drinks then everybody drinks''.

\begin{code}

exampleD =
    search empty ((stab drink ["x"]) :->: (exca ["x"] (drink ["x"] :->: (All ["y"] (drink ["y"])))))
    where
          drink xs = Pred "Q" (depopVarsFAE xs)


\end{code}

\noindent Unlike in the previous examples the proof involves backtracking, as can be seen in steps 11 and 13.

\begin{verbatim}
===Step 0
 Goal:
:-        (all x. Q x -> bottom -> bottom -> Q x) -> 
            (all x. Q x -> (all y. Q y) -> bottom) -> bottom
Stripped implication.
===Step 1
 Goal:
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        (all x. Q x -> (all y. Q y) -> bottom) -> bottom
Stripped implication.
===Step 2
 Goal:
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        bottom
Resolvable with u'
===Step 3
 Goal:
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q X -> (all y. Q y)
Stripped implication.
===Step 4
 Goal:
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        (all y. Q y)
Stripped variable(s).
===Step 5
 Goal:
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y
Resolvable with u
===Step 6
 Goal:
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y -> bottom -> bottom
Stripped implication.
===Step 7
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        bottom
Resolvable with u''' and u'
===Step 8
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y
Resolvable with u
===Step 9
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y -> bottom -> bottom
Stripped implication.
===Step 10
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        bottom
Resolvable with u''' and u'
===Step 11
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y
Failed to resolve.
===Step 11
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q X' [y] -> (all y. Q y)
Stripped implication.
===Step 12
 Goal:
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        (all y. Q y)
Stripped variable(s).
===Step 13
 Goal:
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y'
Failed to resolve.
Failed step 12.
Failed step 11.
Failed step 10.
Failed step 9.
Failed step 8.
===Step 8
 Goal:
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q X' [y] -> (all y. Q y)
Stripped implication.
===Step 9
 Goal:
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        (all y. Q y)
Stripped variable(s).
===Step 10
 Goal:
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y'
Resolvable with u
===Step 11
 Goal:
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y' -> bottom -> bottom
Stripped implication.
===Step 12
 Goal:
u'''''  : Q y' -> bottom
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        bottom
Resolvable with u''''' and u'''
===Step 13
 Goal:
u'''''  : Q y' -> bottom
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y'
Failed to resolve.
===Step 13
 Goal:
u'''''  : Q y' -> bottom
u''''   : Q X' [y]
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        Q y
Resolvable with u''''
Completed step 13.
Completed step 12.
Completed step 11.
Completed step 10.
Completed step 9.
Completed step 8.
Completed step 7.
Completed step 6.
Completed step 5.
Completed step 4.
Completed step 3.
Completed step 2.
Completed step 1.
Completed step 0.
====] Proved [===================
Final sequent was:
u'''''  : Q y' -> bottom
u''''   : Q y
u'''    : Q y -> bottom
u''     : Q X
u'      : (all x. Q x -> (all y. Q y) -> bottom)
u       : (all x. Q x -> bottom -> bottom -> Q x)
:-        bottom

Final context was:
Signatures      : (none)
Flexibles       : X
Forbiddens      : y' and y

Proof   : \u\u'((u' X) \u''\y((u (\yy y)) \u'''((u' (\yy y)) 
           \u''''\y'((u ((\y'\yy' y') y)) \u'''''(u''' u'''')))))
\end{verbatim}

