%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format :=>: = "\Rightarrow"
%format :=*: = "\stackrel{\circ}{\Rightarrow}"
%format VarT x = "\stackrel{\circ}{"x"}"
%format (VarT x) = "\stackrel{\circ}{"x"}"
%format Gamma = "\Gamma"
%format gamma = "\Gamma"
%format gamma2 = "\Gamma^\prime"
%format subst tau1 i tau2 = tau1 "\{" i "\mapsto" tau2 "\}"  
%  overloaded notation from substitution of expressions.
%format Type = "\tau"
%format Type' = "\tau^\prime"
%format tau = "\tau"
%format taupe = "\tau_2"
%format taup = "\tau^\prime"
%format taup1 = "\tau^\prime_1"
%format taup2 = "\tau^\prime_2"
%format t1' = "\tau^{\prime\prime}_1"
%format t2' = "\tau^{\prime\prime}_2"
%format n1 = "n_1"
%format n2 = "n_2"
%format sigma = "\sigma"


\ignore{

>module Types where

>import Data.Char
>import Data.List
>import Expressions
>import General

}

\chapter{Types}

This chapter describes an implementation of the type reconstruction algorithm described by \cite{wand-tinference}. The algorithm
has been modified to work with open terms: this is done by assigning distinct free variables distinct type variables. This assignment corresponds with the principle that in the absence of constraints a variable can have any type.

Wand's algorithm operates in two phases: the first phase involves traversing a term to generate typing constraints; these constraints are then solved using first-order unification.


\section{Type definitions}

The language of types consists of individuals and functions, formalised by the following definition. The overall system will only handle terms typable in this class.
\index{$\tau$}
\index{$\tau$!\emph{$\Rightarrow$}}
\index{$\tau$!\emph{O}}

>data Type 
>          = O
>          | Type :=>: Type

\ignore{

>          deriving (Eq, Show, Read)

}

\noindent The type reconstruction algorithm uses the following types of intermediate values:
\begin{itemize}
\item \emph{TVariable} is the denumerable source of type variables.
\index{\emph{TVariable}}
\ignore{

>type TVariable = Integer

}
\item Type $\tau^\prime$ is the general analogue to $\tau$, or \emph{type schemes}. The symbols $\tau^\prime$ and $\tau$ are used to both represent types and range over their values.
\index{$\tau^\prime$}
\index{$\tau^\prime$!$\stackrel{\circ}{\Rightarrow}$}
\index{$\tau^\prime$!$\stackrel{\circ}{n}$}

>data Type'
>    =  Type' :=*: Type'
>    | VarT TVariable

\ignore{

>           deriving (Show, Eq)

}
\item \emph{TEqn} formalises equations between type schemes. A list of \emph{TEqn} forms the type of unification problems: a \emph{unification problem} is a set of equations.

\index{\emph{TEqn}}

>type TEqn = (Type', Type')

\item \emph{$\Gamma$} is a typing context: a finite map from term variables to types. The symbol $\Gamma$ is used to denote both the type of contexts and also their values.
\index{$\Gamma$}

>type Gamma = (String -> Type')

\item \emph{Goal} values are triples consisting of a typing context, an expression that needs to be typed, and the type calculated up to that point.
\index{\emph{Goal}}

>type Goal = (Gamma, Exp, Type')

\item \emph{Subs} are substitution functions defined over type schemes.
\index{\emph{Subs}}

>type Subs = (Type' -> Type')

\end{itemize}


\section{Constraint generation}

The implementation of the constraint-generation phase follows closely the definition given by \cite{wand-tinference}: it is organised into
an algorithmic \emph{skeleton} that invokes the more specific \emph{actionTable} function. 
The algorithm has been modified to work with open terms in the following way: the function \emph{undefInit} generates an initial typing context by mapping distinct free term variables to distinct type variables, and also provides the function \emph{skeleton} with the seed value for fresh type variable names.
\index{\emph{undefInit}}

\vspace*{1mm}

>undefInit :: Exp -> (TVariable, Gamma)
>undefInit r = 
>              let fvs = (nub.freeVarsExp) r
>                  fvl = ((toInteger(length fvs))::TVariable)
>                  fvl'= map (\x-> VarT x) (countUp fvl)
>                  undef = mapp fvs fvl' --build a map from free variable names to fresh types
>                                        --as far as the type-checker is concerned these would be undefined.
>              in (fvl, undef)

\vspace*{1mm}

The next two functions together generate constraints to be solved by unification.  Function \emph{skeleton} iterates the application of \emph{actionTable} to the initial goal and produces both a unification problem and also the fresh variable name it started with. The latter will be used by the function \emph{typeOf}, defined further down.
\index{\emph{skeleton}}

\vspace*{1mm}

>skeleton :: Exp -> ([TEqn], TVariable)
>skeleton r = 
>             let loop :: ([TEqn],[Goal],TVariable) -> ([TEqn],[Goal],TVariable)
>                 loop (l,[],mt)     = (l,[],mt)
>                 loop (l,(g:gs),mt) = loop (l'++l,gs++g',mt')
>                                      where (l',g',mt') = actionTable g l mt
>                 (eqns,_,_) = loop init --extract the unification problem
>             in (eqns,initv)
>             where 
>                   (initv,undef) = undefInit r --produces initial variable and also initial typing context
>                   g    = (undef,r,VarT initv)  --initial goal; use initial typevar `init`
>                   init = ([],[g],initv+1)  --(init + 1) is the next fresh variable

\vspace*{1mm}

\noindent Function \emph{actionTable} refines goals and, in doing so, produces unification problems.
\index{\emph{actionTable}}

\vspace*{1mm}

>actionTable :: Goal -> [TEqn] -> TVariable -> ([TEqn],[Goal],TVariable)
>actionTable (gamma, Var' x, taup) l mt = ([(taup,gamma x)],[],mt)
>actionTable (gamma, App' r s, taup) l mt = ([],[g1,g2],mt+1)   --adds two new goals
>    where 
>    g1 = (gamma,r,(VarT mt) :=*: taup)
>    g2 = (gamma,s,VarT mt)
>actionTable (gamma, Abs' v r, taup) l mt = ([(taup,(VarT n1) :=*: (VarT n2))],[(gamma2,r,VarT n2)],mt+2)  --refines the goal
>                                     where
>                                           n1 = mt
>                                           n2 = mt+1
>                                           gamma2 = \ x -> if (x == v) then (VarT n1) else (gamma x) --extends the typing context

\vspace*{1mm}

\section{Constraint solution}

The type inference process concludes with the generation of a principal type for the term, if one exists, by solving the constraints generated during the first phase.
A solution to a unification problem is a substitution $\sigma$ such that, for each equation $r \stackrel{?}{=} s$ in the problem, it is the case that $\sigma r \equiv \sigma s$.
\index{\emph{foUnifn}}

\vspace*{1mm}

>foUnifn :: [TEqn] -> Maybe Subs
>foUnifn p =
>    case p of
>      [] -> Just id
>      ((taup1 :=*: taup2, VarT n):es) -> foUnifn ((VarT n, taup1 :=*: taup2):es)
>      ((taup1 :=*: taup2, t1' :=*: t2'):es) -> foUnifn ((taup1,t1'):(taup2,t2'):es)
>      (p@(VarT n, taup1 :=*: taup2):es) ->
>          if occursCheck n (snd p)
>          then Nothing
>          else composeF p n es
>      (p@(VarT n1, VarT n2):es) ->
>          if n1 == n2
>          then foUnifn es
>          else composeF p n1 es
>      where composeF p n es = 
>                let f = subst (snd p) n
>                    g (p1,p2) = (f p1, f p2)  --to map over list of pairs
>                    rest = foUnifn (map g es)
>                in case rest of
>                Nothing  -> Nothing
>                Just s -> Just (s.f)

\vspace*{1mm}


\subsection{Auxilliary functions}

Function \emph{occursCheck} is an elementary check made during unification, \emph{subst} constructs substitutions over type constraints, and \emph{leastInstance} coerces type schemes into simple types -- thus rendering the types, which are calculated by this algorithm, usable by the implementation of normalisation-by-evaluation (Chapter \ref{sec:NbE}).
\index{\emph{occursCheck}}

\vspace*{1mm}

>occursCheck :: TVariable -> Type' -> Bool
>occursCheck i (VarT n) = i == n
>occursCheck i (taup1 :=*: taup2) = (occursCheck i taup1) || (occursCheck i taup2)

\vspace*{1mm}
\index{$\tau \{ i \mapsto \tau_2 \}$}

>subst taup i (VarT n)     = if n == i then taup else VarT n
>subst taup i (taup1 :=*: taup2) = (subst taup i taup1) :=*: (subst taup i taup2)

\vspace*{1mm}
\index{\emph{leastInstance}}

>leastInstance :: Type' -> Type
>leastInstance (VarT n) = O
>leastInstance (t1 :=*: t2) = (leastInstance t1) :=>: (leastInstance t2)


\section{Main function}

Function \emph{typeOf} is the principal export of this module. It first obtains a unification problem and an initial variable from \emph{skeleton}, solves the problem to obtain a substitution function -- if one exists -- using \emph{foUnifn}, and applies this substitution to the initial variable.
\index{\textbf{\emph{typeOf}}}

\vspace*{1mm}

>typeOf r = 
>           let (eqns,init) = skeleton r
>           in case (foUnifn eqns) of
>                Nothing  -> Nothing
>                Just sigma ->Just (sigma (VarT init))



\section{Examples}

\ignore{

>selfApp = (Abs' "x" (App' (Var' "x") (Var' "x")))

>numeral = Abs' "f" (Abs' "x" (App' (Var' "f") (Var' "x")))

>fvar = Var' "x"

>fapp = (App' (Var' "x") (Var' "y"))

>fabs = Abs' "x" (Var' "y")

}

\begin{spec}
typeOf (Abs' "x" (App' (Var' "x") (Var' "x")))
~>Nothing

typeOf (Abs' "f" (Abs' "x" (App' (Var' "f") (Var' "x"))))
~> Just ((VarT 3 :=*: VarT 4) :=*: (VarT 3 :=*: VarT 4))

typeOf (Var' "x")
~> Just (VarT 0)

typeOf (App' (Var' "x") (Var' "y"))
~> Just (VarT 2)

typeOf (Abs' "x" (Var' "y"))
~> Just (VarT 2 :=*: VarT 0)
\end{spec}

