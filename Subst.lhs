%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format `o` = "\fatsemi"
%format doSubst v e m = m "\{" v "\mapsto" e "\}"
%format sigma1 = "\sigma_1"
%format sigma2 = "\sigma_2"

\ignore{

>module Subst where
>import Expressions

}

\section{Substitution} \label{sec:Subst:Subst}
This section defines substitution over \emph{Exp}-values. The equivalent function over \emph{FAE} is definable using the homomorphisms from the previous section. Some operations seemed more natural to define over \emph{Exp} than over \emph{FAE}, and vice versa. For this reason, normalisation (Chapter~\ref{sec:NbE}) is defined over \emph{Exp} and unification (\S~\ref{sec:QUnif}) over \emph{FAE}.
It is useful to assume that, after substitution, terms are brought into long normal form -- this is effected by the functions that invoke the substitution.
The following definition formalises that a value of type \emph{Subst} is the graph of an \emph{Exp}-substitution function.
\vspace*{1mm}

\index{\emph{Subst}}

>type Subst = [(String, Exp)] 

\noindent The next definition interprets a substitution into function over terms in order to facilitate its application.
\index{\emph{mkSubst}}
\index{$r \{ x \mapsto s \}$}

>doSubst v r s = 
>                case s of
>                    Var' x -> 
>                                if x == v
>                                then r
>                                else Var' x
>                    Abs' x t -> 
>                                  if x /= v
>                                  then Abs' x (doSubst v r t)
>                                  else Abs' x t
>                    App' s t -> App' (doSubst v r s) (doSubst v r t)


\noindent Function \emph{mkSubst} lifts the previous definition on single substitutions to lists of substitutions -- it functionalises values of \emph{Subst}. One could expect that \emph{mkSubst} will appear partially-applied in definitions.
\index{\emph{doSubst}}

%>mkSubst :: [(String, Exp)] -> Exp -> Exp 

>mkSubst [] r = r
>mkSubst ((v,r):ss) t = mkSubst ss (doSubst v r t)


\subsubsection{Composition}

The composition of substitutions $\sigma_1$ and $\sigma_2$, denoted by $\sigma_1\fatsemi\sigma_2$, involves the following steps:
\begin{itemize}
\item First, \emph{dropExtra} trims away from $\sigma_2$ any maps that are also defined in $\sigma_1$;
\index{\emph{dropExtra}}

>dropExtra :: Subst -> Subst -> Subst
>dropExtra [] sigma2 = sigma2
>dropExtra sigma1 [] = sigma1
>dropExtra (x:xs) (y:ys) = 
>                          if fst x == fst y
>                          then dropExtra xs (dropExtra (x:xs) ys)
>                          else dropExtra xs (y : (dropExtra (x:xs) ys))

\item Substitution $\sigma_1$ is then applied to the value-terms of $\sigma_2$. This makes use of the function \emph{mkSubst} defined earlier. The definition of composition follows:
\index{$\fatsemi$}

>sigma1 `o` [] = sigma1
>[] `o` sigma2 = sigma2
>sigma1 `o` sigma2 = remSelfMap [(x, mkSubst sigma1 r)  | (x,r) <- (dropExtra sigma1 sigma2)] 


\item The last line of the previous definition refers to a function \emph{remSelfMap}. This function removes maps of the sort $x \mapsto x$ from a substitution.
\index{\emph{remSelfMap}}


>remSelfMap :: Subst -> Subst
>remSelfMap [] = []
>remSelfMap (x:xs) = 
>                  case x of
>                    (v, Var' v') -> 
>                                    if v == v'
>                                    then remSelfMap xs
>                                    else rest
>                    otherwise -> rest
>                  where rest = x:(remSelfMap xs)

\end{itemize}
