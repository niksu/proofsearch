%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format forb q = q "_\forall"
%format sign q = q "_L"
%format addForb q = q "_\forall^+"
%format remForb q = q "_\forall^-"
%format addFlex q = q "_\exists^+"
%format remFlex q = q "_\exists^-"
%format addSign q = q "_L^+"
%format remSign q = q "_L^-"

\chapter {Pattern unification} \label{sec:QUnification}

\ignore{

>module QPrefix (
> QPrefix,
> empty,
> inPrefix,
> addSign,
> addFlex,
> addForb,
> remSign,
> remFlex,
> remForb,
> sign,
> flex,
> forb
>) where

>import Data.List
>import General

}

The proof search algorithm that will be described in the next chapter translates proof problems into unification problems -- more specifically, the unification of \emph{patterns}. Patterns are instances of a restricted class of $\lambda$-terms within a broader class of \emph{Q-terms}. A Q-term is a pair consisting of a quantifier prefix Q and a term whose free variables are captured by the quantifier prefix. A \emph{quantifier prefix} is an instance of a \emph{prefix class}, as used in logic to classify formulas: examples of well-known prefixes include the Bernays-Sch\"{o}nfinkel class $\exists^*\forall^*$ and the Ackermann class $\exists^*\forall\exists^*$, both for FOL. The restricted class from which patterns are drawn is the so-called $L_\lambda$ class: the quantifier prefix for this class is $\forall^*\exists^*\forall^*$. 

Key notions will be formalised next, such as Q-terms and patterns, following which the unification algorithm over patterns is defined. The chapter concludes with applications of the algorithm to some examples. The principal references for this chapter are the articles by \cite{schwicht-psml}, \cite{miller-llambda} and \cite{nipkow1993fuh}. In particular, some of the examples at the end of the chapter were drawn from \citeauthor{nipkow1993fuh}'s article.


\section{Quantifier prefix}

Quantifier prefixes will be restricted to the class $\forall^*\exists^*\forall^*$: the variables bound in each segment are called \emph{signature}, \emph{flexible}, and \emph{forbidden} variables respectively.
A quantifier prefix is implemented here as an abstract datatype. This ensures that it can be built and queried using a restricted set of functions and, consequently, that prefixes are restricted to the class of interest. 
The precise details of the ADT's implementation will be omitted and the behaviour of the interface functions will be summarised instead: nullary \emph{empty} yields a prefix devoid of bound variables; taking $Q$ to range over prefixes and $x$ over variables, $Q_L^+ x$, $Q_\forall^+ x$, and $Q_\exists^+ x$ denote adding $x$ to the signature, flexible, and forbidden variables respectively; 
$Q_L^- x$, $Q_\forall^- x$, and $Q_\exists^- x$ denote removing $x$ from being a signature, flexible, and forbidden variable respectively; and finally $Q_L$, $Q_\exists$, $Q_\forall$ return $Q$'s signature, flexible and forbidden variables respectively as a list.

\index{\emph{QPrefix}}

\index{$Q_L$}
\index{$Q_\exists$}
\index{$Q_\forall$}
\index{$Q_L^+ x$}
\index{$Q_\forall^+ x$}
\index{$Q_\exists^+ x$}
\index{$Q_L^- x$}
\index{$Q_\forall^- x$}
\index{$Q_\exists^- x$}

\ignore{

>newtype QPrefix = QP ([String],[String],[String]) -- AEA Quantifier Prefix


>empty      = QP ([],[],[])
>addSign (QP (sigs,flexs,forbs)) v = QP (v:sigs,flexs,forbs)
>addFlex (QP (sigs,flexs,forbs)) v = QP (sigs,v:flexs,forbs)
>addForb (QP (sigs,flexs,forbs)) v = QP (sigs,flexs,v:forbs)
>remSign (QP (sigs,flexs,forbs)) v = QP (sigs \\ [v],flexs,forbs)
>remFlex (QP (sigs,flexs,forbs)) v = QP (sigs,flexs \\ [v],forbs)
>remForb (QP (sigs,flexs,forbs)) v = QP (sigs,flexs,forbs \\ [v])


>inPrefix (QP (sigs,flexs,forb)) v = (v `elem` sigs) || (v `elem` flexs) || (v `elem` forb)

>sign (QP (sigs,_,_)) = sigs
>flex (QP (_,flexs,_)) = flexs
>forb (QP (_,_,forbs)) = forbs


>instance Eq QPrefix where
> (QP (xs,ys,zs))==(QP (xs',ys',zs')) = (xs==xs')&&(ys==ys')&&(zs==zs')
>instance Show QPrefix where
> show (QP (xs,ys,zs)) = "\nSignatures\t: " ++ (andList xs) ++ 
>                        "\nFlexibles\t: " ++ (andList ys) ++ "\nForbiddens\t: " ++ (andList zs) ++ "\n"

}
