%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\ignore{

>module NbE where
>import Expressions
>import Types
>import General
>import Control.Monad.State

}

%format reflect x t e = "\uparrow_{"t"}^{"x"} "e
%format reify x t e = "\downarrow_{"t"}^{"x"} "e
%format meaning c t = "\llbracket" t "\rrbracket_{"c"}"
%format fresh_to x = "\sharp_"x
%format meaning c ( t ) = "\llbracket" t "\rrbracket_{"c"}"


\chapter{Normalisation by Evaluation} \label{sec:NbE}

In outline, NbE works by first evaluating a term then reifying the resulting value into a term that is $\beta$-normal and $\eta$-long.
The proof search process depends on unification, which is described in the next chapter, and which operates on normal terms. 
The implementation described below closely follows the description on Wikipedia\footnote{\url{http://en.wikipedia.org/wiki/Normalisation_by_evaluation}}, except that it has been modified to work with open terms.


\section{Semantic values}
The meaning of terms consists in functions between semantic values and constants of the individual type: these are named \emph{LAM} and \emph{SYN} respectively. Since we must also cater for open terms, and there is nothing we can assume about free variables, free variables are treated as syntactical thunks in the space of semantic values. These thunks are constructed by \emph{FREE}. As a result of having these additional values, we also need to represent particular applications explicitly in the semantic space: because applying an open term to a -- possibly open -- term cannot otherwise yield a semantic value. The constructor for combinations is \emph{APPLY} and is parametrised by two semantic values and the type of the operand: the type needs to be stored in order to be used by the type-indexed function \emph{reify} when generating a term from a semantic value. This will be elaborated further below.
\index{\emph{Sem}}
\index{\emph{Sem}!\emph{LAM}}
\index{\emph{Sem}!\emph{SYN}}
\index{\emph{Sem}!\emph{APPLY}}
\index{\emph{Sem}!\emph{FREE}}

\begin{code}
data Sem 
    = SYN Exp
    | LAM (Sem -> Sem)
    | APPLY Sem Sem Type --occurs when the operator is a free variable, 
                         --whereupon a LAM cannot be constructed.
    | FREE Exp
\end{code}


\section{Evaluation function}
The function defined next interprets terms into values of \emph{Sem}. This function is called at the start of the NbE process -- cf. the function \emph{nbe} in \S\ref{sec:NbE:main}. As is normally the case, this function is parametrised by a context, as well as the term to be evaluated. 

\newpage

%meaning :: Ctx -> Exp -> Sem
\index{$\llbracket Exp \rrbracket_{Ctxt}$}

\begin{code}
meaning g (Var' x) = lookup_ctx g x
meaning g (Abs' x r) = LAM (\y -> meaning (add_ctx g x y) r) --functions built by threading 
                       --the arguments through the meaning.
meaning g (App' r s) = 
    case meaning g r of
      LAM w -> w (meaning g s)
      FREE t -> APPLY (FREE t) (meaning g s) (leastInstance taup) --this caters for open terms
          where (Just taup) = typeOf s                            --note type info kept in APPLY thunks.
--the following line nests two APPLYs since the first cannot be evaluated 
--functionally (as in the case of (LAM s) above).
      r'@(APPLY _ _ _) -> APPLY r' (meaning g s) (leastInstance taup)
          where (Just taup) = typeOf s

\end{code}

\subsection{Context}

The previous function is parametrised by a context mapping variable names to semantic values. As with substitutions, defined in \S\ref{sec:Subst:Subst}, contexts are represented by their graphs. The implementation is straightforward and the details will be omitted, save that the functions \emph{lookup\_ctx} and \emph{add\_ctx} are defined over the type of contexts.

\index{\emph{empty\_ctx}}
\index{\emph{lookup\_ctx}}
\index{\emph{add\_ctx}}


\ignore{

>type Ctx = [(String, Sem)]

\begin{code}
empty_ctx = []

lookup_ctx :: Ctx -> String -> Sem
lookup_ctx [] c = FREE (Var' c)
lookup_ctx ((v,w):xs) c = 
    if (c == v)
    then w
    else lookup_ctx xs c

add_ctx :: Ctx -> String -> Sem -> Ctx
add_ctx g v w = ((v,w):g)
\end{code}

\begin{code}
fresh_to :: String -> String
fresh_to = (++"'")
\end{code}

}

\section{Reflect and Reify}

Functions \emph{reflect} and \emph{reify} -- denoted by $\uparrow_{t}^{x}$ and $\downarrow_{t}^{x}$ -- are type-indexed functions whose other parameters are the last-used variable $x$, from which to generate fresh variables, and expressions and semantic values respectively.
\index{$\uparrow_{t}^{x} e$}

\vspace*{1mm}

\begin{code}

reflect x O r = SYN r
reflect x (tau :=>: taupe) r = LAM (\w -> reflect x taupe (App' r (reify x tau w)))

\end{code}

\vspace*{1mm}

\noindent The function \emph{reify} has a slightly more complex definition than \emph{reflect} because of its sensitivity to open terms: if we were to restrict our attention to closed terms then the first two clauses of \emph{reify} would have been sufficient. Note that $\sharp_x$ produces the next fresh variable after accepting the current fresh variable $x$.

\index{$\sharp_x$}

\index{$\downarrow_{t}^{x} e$}
\vspace*{1mm}

\begin{code}

reify x O (SYN r) = r

reify x (tau :=>: taupe) (LAM r) = 
    let x' = fresh_to x
        x'' = fresh_to x'
    in Abs' x' (reify x'' taupe (r $ (reflect x' tau (Var' x'))))

reify x tau (APPLY r s taupe) = 
    let x' = fresh_to x
    in App' (reify x (taupe:=>:tau) r) (reify x' taupe s)

reify x tau (FREE r) = r --behaves like SYN

\end{code}


\section{Main function} \label{sec:NbE:main}
The function \emph{nbe} combines the above definitions into an NbE implementation: starting with ``\texttt{a}'' as the first fresh variable, the meaning of expression \texttt{e} of type \texttt{t} is reified.

\vspace*{1mm}

>nbe tau t = reify "a" tau (meaning empty_ctx t)

\vspace*{1mm}

\noindent The principal export of this module is the function \emph{normalisNbE}, which combines the functionality of Wand's algorithm and \emph{nbe}. 
\index{\emph{nbe}}
\index{\textbf{\emph{normalisNbE}}}


\begin{code}
normalisNbE t = 
    case (typeOf t) of
      Nothing  -> Nothing
      Just taup -> Just (nbe (leastInstance taup) t)
\end{code}
          


\section{Examples}

The next set of definitions are drawn from the Wikipedia article cited earlier too.

\begin{code}


k = Abs' "x" (Abs' "y" (Var' "x"))
s = Abs' "x" (Abs' "y" (Abs' "z" (App' (App' (Var' "x") (Var' "z")) (App' (Var' "y") (Var' "z")))))
skk = App' (App' s k) k
testrun = nbe (O :=>: O) skk


\end{code}

\noindent Some results of the NbE implementation are presented next.

\begin{spec}
testrun
~>Abs' "a'" (Var' "a'")

normalisNbE (App' (Var' "F") skk)
~>Just (App' (Var' "F") (Abs' "a'" (Var' "a'")))

normalisNbE (App' skk (Var' "F"))
~>Just (Var' "F")

normalisNbE (Abs' "x" (App' skk (Var' "F")))
~>Just (Abs' "a'" (Var' "F"))
\end{spec}


\addcontentsline{toc}{section}{Notes}
\section*{Notes}
The implementation would benefit, in terms of clarity, from a `cleaner' handling of fresh variables.
It can be rendered more generic by making abstract the type of names, and turning \emph{Ctx} into an ADT. 

