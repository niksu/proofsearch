%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format bottom = "\bot"
%format exca x y = "\exists^\mathrm{cl}"x"\ "y

\ignore{

>module Prover where
>import QPrefix
>import Subst
>import Types
>import Terms
>import NbE
>import Expressions
>import QUnification
>import ProverData
>import General


>import Data.Char
>import Data.List
>import Data.Maybe --for catMaybes
>import Control.Monad

}


\subsection{Harrop normal form}

For the purposes of resolution it is useful to represent antecedent formulas as $\forall \vec{x}. \vec{f} \longrightarrow g$, called an \emph{elab-list} in the Minlog implementation and \emph{Harrop normal form} (HNF) in Isabelle \citep[\S2.3.1]{berghofer:ppa}. This representation induces a structural uniformity in formulas, as a result of which checking whether the formula's head (conclusion) and the sequent's conclusion are resolvable. If the conclusions can be resolved then $\vec{f}$ are added as subgoals to the proof process.
Formulas in HNF are formalised as values of the type \emph{Reformula} below. The functions \emph{reformulate} and \emph{unreformulate} are defined to handle conversions of formulas to and from values of \emph{Reformula} respectively; their definitions are straightforward and are omitted here.

\index{\emph{Reformula}}

\begin{code}

type Reformula = ([String],[Formula],Formula)

\end{code}

\index{\emph{reformulate}}
\index{\emph{unreformulate}}


\ignore{

\begin{code}

reformulate :: Formula -> Reformula
reformulate f@(Pred _ _)   = ([],[],f)
reformulate (All xs f)     = 
    let (assumptions, conc) = reformulate' f
    in (xs,assumptions,conc)
reformulate f@(_ :->: _) = 
    let (assump,conc) = reformulate' f
    in ([],assump,conc)


reformulate' :: Formula -> ([Formula],Formula)
reformulate' f@(Pred _ _) = ([],f)
reformulate' f@(All _ _)  = ([],f)
reformulate' (f :->: f')  = 
    let (assumptions, conc) = reformulate' f'
        assumptions' = (f:assumptions)
    in (assumptions',conc)


unreformulate :: Reformula -> Formula
unreformulate (xs,ass,conc) = 
    if (xs == [])
    then rest
    else All xs rest
    where rest = foldr (:->:) conc ass

\end{code}
}


\section{Theorem prover}

The proof system consists of two inference rules: \emph{strip} and \emph{resolve}. The proving machinery applies these
automatically by iterating a process that checks the shape of the formula then applies one of these two rules, and fails if the inference step fails. Function \emph{prove} accepts a Q-sequent and if the search is successful it returns the final Q-sequent, a proof and the overall substitution function computed.

\index{\emph{prove}}

\begin{code}

prove :: Int -> QSequent -> IO (Maybe (QPrefix, Subst, Proof, Sequent))
prove cnt (q, s) = do
  putStr ("===Step "++(show cnt)++"\n Goal: "++(show s)++"\n")
  case s of
    p :- All xs f  -> 
        case (strip cnt (q,s)) of
          Nothing -> do
             putStr ("Failed to strip variable(s).\n")
             return Nothing
          Just (q',s',pf,rho) -> do
             putStr ("Stripped variable(s).\n")
             continuance <- prove (cnt+1) (q',s')
             case continuance of --do rest of the proof
               Nothing -> do
                  putStr ("Failed step "++(show cnt)++".\n")
                  return Nothing
               Just (q2,phi',pf_rest,s'') ->
                  let 
                      phi = restrict (flex q') (rho `o` phi')
                      goal = appSubstSequ phi s''
                  in do
                      putStr  ("Completed step "++(show cnt)++".\n")
                      return $ Just (q2,phi,toCommonNotation (fnise phi (toFunctorArguments (pf pf_rest))), goal)
    p :- (f :->: f') -> 
        case (strip cnt (q,s)) of
          Nothing -> do
             putStr ("Failed to strip implication.\n")
             return Nothing
          Just (q',s',pf,rho) -> do
             putStr ("Stripped implication.\n") 
             continuance <- prove (cnt+1) (q',s')
             case continuance of --do rest of the proof
               Nothing -> do
                  putStr ("Failed step "++(show cnt)++".\n")
                  return Nothing
               Just (q2,phi',pf_rest,s'') ->
                  let
                      phi = restrict (flex q') (rho `o` phi')
                      goal = appSubstSequ phi s''
                  in do
                      putStr ("Completed step "++(show cnt)++".\n")
                      return $ Just (q2,phi,toCommonNotation (fnise phi (toFunctorArguments (pf pf_rest))), goal)
    p :- Pred n ts -> 
        case (resolve cnt (q,s)) of
          [] -> do
            putStr ("Failed to resolve.\n")
            return Nothing
          list -> 
              let

                  ploop [] = return []   --iterate through possible paths until reach one that leads to the proof.
                  ploop (x@(q,ss,pf,_,_):xs) = 
                      let
                          loop [] _ = return [] --no subgoals
                          loop (x:xs) q = do
                            continuance <- prove (cnt+1) (q,x)
                            case continuance of --do rest of the proof
                              Nothing -> 
                                  return [Nothing] --abort the proof attempt, disregard other subgoals.
                              ans@(Just (q',rho,pf',_)) ->
                                  let
                                      xs' = map (appSubstSequ rho) xs
                                  in if xs==[]
                                     then return [ans]
                                     else do
                                           rest <- loop xs' q'
                                           return (ans:rest)
                          theRest y = if not (Nothing `elem` y)
                                      then return [(x,y)] --don't attempt the other paths, 
                                                          --return the first that succeeds.
                                      else ploop xs
                      in do
                          thisGoal <- loop ss q
                          theRest thisGoal

                  attempt = ploop list

              in do
                  putStr ("Resolvable with "++(andList (map (\(_,_,_,_,u)->u) list))++"\n") 
                  --try each resolvant in turn, until we reach one for which all subgoals are provable.
                  attemptResult <- attempt
                  case attemptResult of
                    [] -> do
                      putStr ("Failed step "++(show cnt)++".\n")
                      return Nothing   --none of the proof attempts succeeded.
                    _  -> 
                        let
                            focus = head attemptResult
                            (q,ss,pf,subs,_) = fst focus --original goal information

                            answer x = 
                                let
                                    result = catMaybes x  --focus on first successul attempt
                                in
                                  if (result == [])
                                  then 
                                      if (ss /= [])
                                      then error "Goal mismatch!\n"
                                      else --apply preproof to empty subproof list
                                           Just (q, subs, pf [], appSubstSequ subs s)
                                  else
                                      let
                                          finalStep = last result 
                                          --finalStep contains the final state and final substitution
                                          --that we're to use. Now concentrate on building proof term.
                                          (q',phi',_, p' :- _) = finalStep
                                          proofs = map (\(_,_,pf_i,_)-> pf_i) result --project out the list of proofs
                                          phi = restrict (flex q') subs `o` phi'  
                                      in Just (q', phi, toCommonNotation ( (fnise phi) (toFunctorArguments (pf proofs))), appSubstSequ phi (p' :- (Pred n ts)))

                        in do
                            putStr ("Completed step "++(show cnt)++".\n")
                            return (answer (snd focus))


\end{code}


\subsection{Strip}

The rule \emph{strip} fuses the introduction rules for quantification and implication and could be illustrated as shown below. 
The inference forms part of a proof tree that is rooted at the top.
Preceding the inference is its context on the left of the bar, and following the inference is the new context on the left of the bar and the proof sought on the right.
Note that the line following the inference is only valid if the rest of proof is possible -- so in this sense this illustration is unlike standard descriptions of inference rules. The rest of the proof process serves to define the proof term $M$ and the substitution $\phi'$.
The symbol $\phi$ abbreviates $(\rho \fatsemi \phi') \restriction Q_\exists$; additional symbols are explained in the column on the right of the rule.


\begin{center}
\begin{tabular}{ll}
$\inferrule{\begin{array}[c]{l} \makebox[22mm]{$Q,\Lambda$} \mid \makebox[20mm]{ } \cr \makebox[13mm]{ } \forall \vec{x}. \vec{D} \longrightarrow A\end{array}}{\begin{array}[c]{l} \makebox[10mm]{ } \makebox[26mm]{$A\rho$} \cr \makebox[22mm]{$Q_\forall^+ \vec{y},\Lambda\lambda\vec{y}\lambda\vec{u}^{\vec{D}\phi}$} \mid \makebox[20mm]{ $M^{A\phi}$ } \end{array}}$%
&%
\begin{minipage}{5cm}
The $\vec{u}$ are fresh assumption variables that are associated with $\vec{D}$ ordinate-wise. The list 
$\vec{y}$ abbreviates $\vec{x}\rho$, where $\rho$ is a renaming that avoids shadowing in $\Lambda$, which would lead to variable capture in $M$.
\end{minipage}
\end{tabular}
\end{center}

The rule above is automatically applied to whittle a goal while gradually building its proof term. The rest of the proof process attempts to define $M$. The precise definition of the inference rule is presented next.

\vspace*{1mm}

\index{\emph{strip}}

\begin{code}

strip :: Int -> (QPrefix, Sequent) -> Maybe (QPrefix, Sequent, Exp -> Proof, Subst)
--return new prefix, additional proof goals, a function to build proof (when the new proof goals have 
--been dischanged), and a substitution in case any bound variables need renaming.
strip cnt (q, p :- f) = 
    case f of
      All xs f ->
          let
          --now ensure that xs is disjoint from all names in use so far
          xs' = 
              let
                  namesSoFar = 
                      let 
                          assumpVars = map (\(x,_,_)->x) p
                          prefixVars = sign q ++ flex q ++ forb q
                      in assumpVars ++ prefixVars
              in freshen xs namesSoFar
          renaming = 
              let 
                  ren1 = zip xs (map toCommonNotation (depopVarsFAE xs'))
                  loop [] = []
                  loop (this@(x, Var' y):rest) = 
                      if x == y   --remove (x,Var x) substitutions
                      then loop rest
                      else this:(loop rest)
              in loop ren1
          --forbid the variables
          q' = foldl (addForb) q xs'
          --produce subgoal
               
          in Just (q', (p :- appSubstForm renaming f), \t-> toCommonNotation (Abs xs' (toFunctorArguments t)), renaming)

      f1 :->: f2 ->
          let
              u' = --come up with fresh name for this assumption
                   let
                       assumpVars = map (\(x,_,_)->x) p
                   in head $ freshen ["u"] assumpVars
              p' = --check if f1's already in ``p'', if so then increment its availability otherwise add it.
                   case lookup f1 (map (\(x,y,z)->(y,(x,z))) p) of
                     Nothing -> (u',f1,2):p
                     Just (u,n) -> replace (u,f1,n) (u,f1,n+2) p

          in Just (q, p' :- f2, \t-> Abs' u' t, [])

\end{code}


\subsection{Resolve} \label{sec:PD:resolve}

The rule \emph{resolve} fuses the elimination rules for quantification and implication. In the illustration below, prior to the inference being made, a suitable assumption needs to be chosen to be resolved with the current proof goal. 
Potentially, there might be more than one plausible candidates for $u$: in such a case the candidates are tried in turn until a proof is found in depth-first-search fashion.


\begin{center}
\begin{tabular}{ll}
$\inferrule{\begin{array}[c]{l} \makebox[25mm]{$Q,\Lambda \ni u^{\forall\vec{x}. \vec{G} \longrightarrow P\vec{s}}$} \mid \makebox[30mm]{ } \cr \makebox[25mm]{ } P \vec{r}\end{array}}{\begin{array}[c]{l} \makebox[23mm]{ } \vec{G}^*\rho \cr \makebox[25mm]{$Q',\Lambda^{(\rho \fatsemi \phi') \restriction Q' }$} \mid u\bullet \vec{x}^*\rho\phi'\bullet\vec{M}^{\vec{G}^*\rho\phi'} \end{array}}$%
&%
\begin{minipage}{5cm}
The $\vec{M}$ are proofs of $\vec{G}^*\rho\phi'$ ordinate-wise, and $\vec{X}$ are fresh variables equinumerous to $\vec{x}$. The map $\cdot^*$ is $x_i \mapsto X_i \bullet Q_\forall$. Crucial to this inference step is the result of unification

$(Q',\rho) := \mathrm{unify} \; (Q^+_\exists \vec{X}) \; (\vec{r},\vec{s}^*)$ 

\end{minipage}
\end{tabular}
\end{center}

The subterm $\vec{x}^*\rho\phi'$ is equivalent to $((\vec{X}\rho\phi')\bullet Q_\forall)$ and serves to eliminate $\forall\vec{x}$ in $u$. The $\vec{M}$ then discharge the assumptions $\vec{G}$ in $u$.


\vspace*{1mm}

\index{\emph{resolve}}

\begin{code}


resolve :: Int -> (QPrefix, Sequent) -> [(QPrefix, [Sequent], [Exp] -> Proof, Subst, String)] 
--Last component of function's result is the assumption variable of the formula resolved with.
resolve cnt (q, p :- Pred n rs) = 
    let 
        resolvants = tryMatching (q, p :- Pred n rs)
        loop [] = []
        loop ((f,f'@(xs,gs,c),p',(q',rho), allElims):rest) = 
            (q', ss', proof, rho, u):(loop rest)
            where 
              u = let 
                      a's = map (\(x,y,_)->(y,x)) p'
                  in 
                    case lookup f a's of
                       Just identifier -> identifier    
                       Nothing -> --This should never be the case. 
                                  error ("Could not find assumption "++(show f')++"\nin "++(show a's)++"\n")                        
              ss' = map (appSubstSequ rho) (map (\g-> p' :- g) gs)    --new subgoals
              proof = --feed it subgoals' proofs to obtain proof of original goal.
                      \x -> toCommonNotation (App (App (Var u) allElims) (map toFunctorArguments x)) 
    in loop resolvants
\end{code}

\subsubsection{Finding resolvants}

The function \emph{tryMatching} sifts the antecedents in a sequent and returns a list of resolvable candidates.
This process is organised into two stages:
\begin{enumerate}
\item Antecedents which do not conclude with the goal predicate are filtered off;
\item The goal's arguments and those of the antecedent's conclusion are unified. If this succeeds then the outcome is retained, otherwise the resolution candidate is dropped.
\end{enumerate}

In effect this function returns a list of possible paths to follow in search of the proof. These are tried in turn; when all the sub-paths leading from a path do not lead to a proof then the system chronologically backtracks and searches along the next path.

\vspace*{1mm}

\index{\emph{tryMatching}}

\begin{code}

tryMatching :: (QPrefix, Sequent) -> [(Formula, Reformula, [(String,Formula,Int)], (QPrefix, Subst), [FAE])]
tryMatching (q, p :- Pred n rs) =
    let
        loop [] = []
        loop ((u,f,cnt):xs) = 
            if cnt>0 --heed multiplicity of use of assumption
            then
                let f'@(vars,ante,conc) = reformulate f
                in case conc of
                     (Pred m ts') ->
                         if (n == m) && (length rs)==(length ts')
                         then 
                             let
                                 p' = --decrement availability of this assumption
                                      replace (u,f,cnt) (u,f,cnt-1) p
                             in
                               (f, f', p', (q, (rs, ts'))):(loop xs)
                         else loop xs
                     _ -> loop xs
            else loop xs
        filter1 = loop p --first round of checks

        prop = 
            --this definition shows there is lots of processing to do before calling unify
            \(f, f'@(xs,fs,c), ante, p@(q, (rs,ts)))-> 
               let
                   namesSoFar = 
                       let 
                           assumpVars = map (\(x,_,_)->x) ante
                           prefixVars = sign q ++ flex q ++ forb q
                       in assumpVars ++ prefixVars

                   xs' = freshen xs namesSoFar

                   renaming = 
                       let ren1 = zip xs xs'
                           loop [] = []
                           loop ((x, y):rest) = 
                               if x == y   --remove (x,Var x) substitutions
                               then loop rest
                               else (x, Var' y):(loop rest)
                       in loop ren1

                   fs' = map (appSubstForm renaming) fs
                   c'  = appSubstForm renaming c   
                   --up to now have ensured that variable names occurring resolvant and in 
                   --current goal are disjoint. The unification problem is prepared next; 
                   --now we must synch ``ts'' by applying the renaming defined previously.

                   ts' = map (fnise renaming) ts

                   namesSoFar' = namesSoFar ++ xs'

                   --and now for the processing prescribed in the paper.
                   zs = depopVarsFAE (forb q)
                   xS = freshen (map (map toUpper) xs') namesSoFar' --fresh names for raised variables
                   qStar = foldl (addFlex) q xS
                   valueTerms = map (\x'-> (App (Var x') zs)) xS   --value terms of the mapping defined next
                   star = map (\(x,x')-> (x, toCommonNotation x')) (zip xs' valueTerms)
                   tsStar = map (fnise star) ts'

                   gStar = map (appSubstForm star) fs'
                   c'' = appSubstForm star c'

                   unifProb = zip (map (compact) rs) (map (compact) tsStar)
                   p' = unify (qStar, unifProb)        --try to solve the unification problem built until now.
               in case p' of
                    Nothing -> Nothing
                    Just (q', rho) -> 
                        let
                            f' = reformulate (appSubstForm rho (unreformulate (xs',gStar,c'')))
                            allElims = --this is used in building the proof term, in elimination of forall
                                       map (fnise (renaming`o`rho)) valueTerms 

                        in Just (f, f', ante, (q', renaming++rho), allElims)

        filter2 = catMaybes (map prop filter1)

    in filter2




\end{code}



\section{Auxilliary functions}
The combinators \emph{appSubstForm} and \emph{appSubstSequ} are defined to apply substitutions to formulas and sequents respectively.
The function \emph{freshen} ensures that two lists of names are disjoint by replacing common names in the second list with fresh names. The precise implementation details are omitted here.

\index{\emph{appSubstForm}}
\index{\emph{appSubstSequ}}
\index{\emph{freshen}}


\ignore{

\begin{code}

--apply a substitution to a formula
appSubstForm rho f = compact1 (normalise (liftExpForm (toCommonNotation.(fnise rho).toFunctorArguments) f))
--and to a sequent
appSubstSequ rho s = compact1 (normalise (liftFrmtoSeq (liftExpForm (toCommonNotation.(fnise rho).toFunctorArguments)) s))

--ensure a list of names if fresh relative to another list, by carrying out necessary alpha conversions.
freshen [] names = []
freshen (x:xs) names = 
    if (x `elem` names) || (x `elem` xs)
    then 
        let x' = x ++ "'"
        in freshen (x':xs) names
    else x : (freshen xs names)

\end{code}
}


\section{Main function}

The key function in this implementation is \emph{search} and it starts the proof process after being provided with an initial quantifier prefix (usually \emph{empty}) and a formula. Examples of output produced are provided in Appendix~\ref{ch:Examples}.

The theorem prover produces output in two stages:
\begin{enumerate}
\item During the proof search process the prover reports each inference step it makes, the goal it is tackling, and whether the inference is successful. This trace serves to explain how a proof was obtained, or why none was found.
\item If a proof has been found then the prover outputs both contexts -- i.e., assumption and quantifier prefix contexts -- and the proof term.
\end{enumerate}

\index{\textbf{\emph{search}}}

\ignore{

\begin{code}

search qpref f = 
    do
      result <- prove 0 (qpref, [] :- f)
      case result of
              Nothing -> 
                  putStr "Could not find a proof.\n"
              Just (q,subs,pf,sequ) ->
                  putStr ("====] Proved [===================\n" ++ "Final sequent was:"++ (show sequ)++"\n\n" ++ "Final context was:" ++ (show q) ++ "\nProof\t: "++ (show pf) ++ "\n\n")
                  
           
\end{code}

}

