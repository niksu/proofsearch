%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format Var' x = x
%format Abs' x e = "\lambda\!\!\lambda" x "\ " e
%format App' e1 e2 = e1 "\bullet" e2
%format Var x = x
%format Abs x e = "\lambda\!\!\lambda" x "\ " e
%format App e1 e2 = e1 "\bullet" e2
%format (Var' x) = x
%format (Var x) = x



\chapter{Terms}
\ignore{

>module Expressions where
>import Data.List

}

This chapter will describe the implementation of $\lambda$-term representations and some standard definitions over them. 
These term representations are inter-translatable and will be instantiated in a typeclass called \emph{Terms} in \S\ref{sec:class:term}.
In the final chapter formulas and sequents too will be instantiated in this class to facilitate their handling.
The definitions in this chapter lay the foundations for the chapters that follow, and it concludes with a definition of substitutions and their composition.


\section{Representation} \label{sec:Representation}
Two representations for expressions will be defined, both of which use concrete names for variables. At times it is more convenient to use one representation over the other. The first definition consists of the standard notation for $\lambda$-expressions.
\index{\emph{Exp}}
\index{\emph{Exp}!$x$}
\index{\emph{Exp}!$\lambda\!\lambda$}
\index{\emph{Exp}!$\bullet$}

\ignore{ 

>infix 9 `Var'`
>infix 8 `Abs'`
>infixl 5 `App'`
>infix 9 `Var`
>infix 8 `Abs`
>infixl 5 `App`

}

>data Exp 
>         = Var' String                -- variables
>         | Abs' String Exp            -- abstraction
>         | App' Exp Exp               -- application

\ignore{ 

>         deriving (Eq, Read)

}

\noindent The second definition uses \emph{functor-arguments} notation -- that is, the operands in nested applications are collected into a list of operands, as are the variable names in nested abstractions. The symbols used above to denote object-level abstraction and application are overloaded, but the intended representation should be clear from the context of use.

\index{\emph{FAE}}
\index{\emph{FAE}!$x$}
\index{\emph{FAE}!$\lambda\!\lambda$}
\index{\emph{FAE}!$\bullet$}

>data FAE 
>         = Var String
>         | Abs [String] FAE
>         | App FAE [FAE]

\ignore{ 

>         deriving (Eq, Read)

\begin{code}

instance Show Exp where
 show (Var' x) = x
 show (Abs' x t) = "\\" ++ x ++ "" ++ (show t)
 show (App' t t') = "(" ++ (show t) ++ " " ++ (show t') ++ ")"


instance Show FAE where
 show (Var x) = x
 show (Abs xs t) = "\\" ++ (xsPrint xs) ++ "" ++ (show t)
                   where xsPrint [] = ""
                         xsPrint (x:xs) = case xs of
                                           [] -> x
                                           _  -> x ++ "," ++ (xsPrint xs)
 show (App t ts) = "(" ++ (show t) ++ " " ++ (show ts) ++ ")"

\end{code}
}
\ignore{ 

\subsection{Standard definitions}

Two standard functions over terms are defined below. A class of types will be defined in the next section
and both term representations will be instantiated in it.
These will be used to instantiate the representations given above in the class of terms.

\index{$\mathsf{FV}_\mathrm{Exp}$}
\index{$\mathsf{BV}_\mathrm{Exp}$}
\index{$\mathsf{FV}_\mathrm{FAE}$}
\index{$\mathsf{BV}_\mathrm{FAE}$}


>freeVarsExp (Var' v) = [v]
>freeVarsExp (Abs' x e) = (freeVarsExp e) \\ [x]
>freeVarsExp (App' m n) = (freeVarsExp m) ++ (freeVarsExp n)

\vspace*{1mm}

>boundVarsExp (Var' v) = []
>boundVarsExp (Abs' x e) = x:(boundVarsExp e)
>boundVarsExp (App' m n) = (boundVarsExp m) ++ (boundVarsExp n)


\noindent Similar definitions for FAE are given next.

>freeVarsFAE (Var v) = [v]
>freeVarsFAE (Abs vs e) = (freeVarsFAE e) \\ vs
>freeVarsFAE (App m n) = (freeVarsFAE m) ++ (concatMap freeVarsFAE n)

\vspace*{1mm}

>boundVarsFAE (Var v) = []
>boundVarsFAE (Abs vs e) = vs ++ (boundVarsFAE e)
>boundVarsFAE (App m n) = (boundVarsFAE m) ++ (concatMap boundVarsFAE n)


}

