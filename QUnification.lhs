%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

%format popVarsFAE x = "\ulcorner" x "\urcorner"
%format `intersect` = "\cap"
%format union = "\cup"
%format subtermOf x y = x "\subseteq_\Lambda" y
%format rawEtaEx ( x ) = x
%format freshFrom x y = "\sharp_{" y "}" x
%format depopVarsFAE x = "\ulcorner\!\ulcorner" x "\urcorner\!\urcorner"
%format popVarsExp x = "\ulcorner" x "\urcorner"
%format depopVarsExp x = "\ulcorner\!\ulcorner" x "\urcorner\!\urcorner"
%format fnise x = "\overline{" x "}^\mathrm{fun}"
%format (fnise x) = "\overline{" x "}^\mathrm{fun}"

\ignore{

\begin{code}
{-# OPTIONS_GHC -fglasgow-exts #-}   

module QUnification where

import Data.List

import QPrefix
import Subst
import Types
import Terms
import NbE
import Expressions
import General

import System.IO.Unsafe

\end{code}

}


\section{Q-Terms}

A Q-term is formalised as a pair consisting of a quantifier prefix and a term. In unification one tends to work with terms represented in functor-arguments notation, which was defined in \S\ref{sec:Representation}. Q-terms are instantiated in the class \emph{Term}, which was defined in \S\ref{sec:class:term}, as shown below.

\index{\emph{QTerm}}
\index{\emph{Term}!\emph{QTerm}}

\ignore{ 

>type QTerm = (QPrefix,FAE)

}

>instance Term QTerm where
>  freeVars (q,t) = freeVars t \\ (sign q ++ flex q ++ forb q)
>  boundVars (q,t) = nub $ boundVars t ++ sign q ++ flex q ++ forb q
>  compact (q,t) = (q,compact t)
>  compact1 (q,t) = (q,compact1 t)
>  normalise (q,t) = (q, normalise t)



\section{Patterns} \label{sec:patterns}

Patterns are Q-terms for which the quantifier prefix is $\forall^*\exists^*\forall^*$ and whose terms are restricted as follows:
\begin{itemize}
\item if $u$ is forbidden in Q and $\vec{r}$ are Q-terms, then $u\bullet\vec{r}$ is a Q-term;
\item if $Y$ is flexible in Q and $\vec{z}$ are distinct variables forbidden in Q, then $Y\bullet\vec{z}$ is a Q-term;
\item if $r$ is a Q$_\forall^+ z$-term then $\lambda\!\!\lambda z\; r$ is a Q-term.
\end{itemize}
Also note that patterns are higher-order: flexible variables may be function variables.
The function \emph{isPattern} detects whether a given Q-term is a pattern -- or equivalently, an element in $L_\lambda$.
In order to be checked, a Q-term must be in compact form: this ensures uniform treatment.
The checking function sees to normalising a Q-term before applying tests to check whether it is a pattern.
It disregards the quantifier prefix and focuses on the structure of terms, since by the definition of Q-terms the quantifier prefix is restricted to a particular class.

\index{\emph{isPattern}}

\begin{code}
isPattern :: QTerm -> Bool
isPattern (q, Var z) = z `elem` (sign q ++ flex q ++ forb q)
isPattern (q, Abs z r)     = isPattern (foldl (addForb) q z, r)
isPattern (q, App (Var u) r) = 
                              if u `elem` sign q || u `elem` forb q
                              then let r' = map (\x -> (q,x)) r   --turn each term in ``r'' into a Q-term
                                   in foldr (&&) True (map isPattern r')  --check that operands are patterns
                              else if u `elem` flex q
                                   then let 
                                            --forbd tests that each element in list is a forbidden var.
                                            forbd l    = foldR test True l
                                                         where test z xs next= 
                                                                case normalise (rawEtaEx (toCommonNotation z)) of
                                                                     Abs' z1 (App' (Var' z'') (Var' z2)) ->
                                                                         if z1 == z2
                                                                         then z'' `elem` forb q && next
                                                                         else False
                                                                     otherwise -> False
                                           
                                        in distinct r && forbd r
                                   else False -- since ``u''  does not appear in quantifier prefix
isPattern _ = False


\end{code}



\section{Pattern unification} \label{sec:QUnif}
The Q-prefix carries over to unification problems too: a \emph{Q-unification problem} is a unification problem in the context of quantifier prefix Q and is formalised as follows:

\index{\emph{UProblem}}
\index{\emph{Eqns}}

\begin{code}
type UProblem = (QPrefix, Eqns FAE)     
type Eqns a = [(a,a)]
\end{code}

Values of \emph{UProblem} could also be specified to be a list of Q-terms, but it is more convenient to work with the definition given above since it reflects the expectation that all the Q-terms share the same prefix.
It has been proved, in the articles cited earlier, that the unification problem for $L_\lambda$ is decidable, yields most general unifiers, and that its quantifier prefix class is closed under unification.
The type \emph{UProblem} is made an instance of \emph{Term}; the details are omitted since the instantiation is straightforward.

\index{\emph{Term}!\emph{UProblem}}

\ignore{
\begin{code}

instance Term UProblem where
  freeVars  (q,es) = foldr union [] (map (\(x,y)-> (freeVars x)++(freeVars y)) es)
  boundVars (q,es) = foldr union [] (map (\(x,y)-> (boundVars x)++(boundVars y)) es)
  compact   (q,es) = (q,map (\(x,y)-> (compact x, compact y)) es)
  compact1   (q,es) = (q,map (\(x,y)-> (compact1 x, compact1 y)) es)
  normalise (q,es) = (q,map (\(x,y)-> (normalise x, normalise y)) es)

\end{code}
}

A unification problem will be solved iteratively by a function of type $UProblem \rightharpoonup (UProblem, Subst)$. Solving the problem 
involves refining the problem and accumulating a substitution function. This process terminates either when the problem is trivial -- that is, all its equations are identities -- or when a solution cannot be found. If the problem is solvable, the substitutions of sub-problems are composed to yield a unifier for the problem we started with.
A successful solution process could be illustrated as shown below, where $U_0$ denotes the original Q-unification problem, and $U_i$ the $i$th refinement.

\[
U_0 \longrightarrow_{\rho_1} U_1 \longrightarrow_{\rho_2} U_2 \longrightarrow_{\rho_3} \cdots \longrightarrow_{\rho_{n-1}} U_{n-1} \longrightarrow_\varepsilon U_n
\] 

Let $\rho_n$ be $\varepsilon$. The solution, denoted by $\phi$, to $U_0$ is then $(\rho_1\fatsemi\cdots\fatsemi\rho_n)\restriction Q_\exists$. Given the iterative nature of the algorithm $\phi$ can be expressed as $(\rho_1\fatsemi\phi')\restriction Q_\exists$ where $\phi'$ is the solution to $U_1$.
The pattern unification algorithm is defined next by cases on the shape of its input.


\begin{code}
unify' :: UProblem -> Maybe (UProblem, Subst)
unify' p@(q,[]) = Just (p,[])                               -- case: trivial
unify' p@(q,(e@(r,s):es)) = 
    if r == s                   -- case: identity
    then Just ((q,es),[])
    else case (r,s) of
           (Abs x r', Abs y s') -> 
               if x /= y       -- case: xi
               then Nothing
               else Just ((q', ((r',s'):es)),[])
                   where q' = foldl (addForb) q x
           (App (Var f) r', App (Var g) s') ->
               if (f `elem` sign q && g `elem` sign q)||(f `elem` forb q && g `elem` forb q)        -- case: rigid-rigid
               then 
                   if f /= g
                   then Nothing
                   else 
                        let r's' = zip r' s'
                        in Just ((q,(r's'++es)),[])
               else 
                   if (f `elem` sign q || f `elem` forb q) && g `elem` flex q   -- case: rigid-flex
                   then Just ((q, ((swap e):es)),[])
                   else unify'' p                  -- hand over to handle other cases.
           otherwise -> unify'' p

unify'' (q,((App (Var f) r', App (Var g) s'):es)) = 
    if f `elem` flex q && g `elem` flex q        -- case: flex-flex
    then 
        if f == g
        then 
            let f'  = freshFrom (flex q) f
                q'  = addFlex (remFlex q f) f'
                rho = [(f, toCommonNotation (Abs (popVarsFAE r') (App (Var f') w)))]
                w   = r' `intersectPT` s'
            in Just ((q', mapPair (fnise rho) es), rho)
        else --with different heads
            let f'  = freshFrom (flex q) f
                q'  = addFlex (remFlex (remFlex q f) g) f'
                rho = [(f, toCommonNotation (Abs (popVarsFAE r') (App (Var f') w))), (g, toCommonNotation (Abs (popVarsFAE s') (App (Var f') w)))]
                w   = r' `intersect` s'
            in Just ((q', mapPair (fnise rho) es), rho)
    else unify''' (q,((App (Var f) r', App (Var g) s'):es))

unify''  p = unify''' p

unify''' (q,((App (Var f) r', t):es)) = 
    if not (f `elem` flex q)                                  -- case: flex-rigid
    then Nothing
    else 
        if subtermOf (Var f) t         --occurs check
        then Nothing
        else 
             if shouldPrune q r' t     --pruning necessary
             then case canPrune (q,t) [] of
                     Nothing           -> Nothing   --pruning failed
                     Just ([(v,e)],v') -> 
                         let q'  = addFlex (remFlex q v) v'
                             rho = [(v,e)]
                         in Just ((q', mapPair (fnise rho) es), rho)
             else                        --explicit definition
                  let q'  = remFlex q f
                      rho = [(f, toCommonNotation (Abs (popVarsFAE r') t))]
                  in Just ((q', mapPair (fnise rho) es), rho)

unify''' _ = Nothing



\end{code}

\subsection{Pruning}

The flex-rigid case in the unification process may involve pruning away free -- actually, \emph{loose} -- forbidden variables if they appear on only one side of an equation. Function \emph{shouldPrune} tests whether pruning is needed, and \emph{canPrune} attempts to prune. If the former is true but the latter fails then the unification process fails.

\index{\emph{shouldPrune}}

\begin{code}

shouldPrune :: QPrefix -> [FAE] -> FAE -> Bool
shouldPrune q l t = (shouldPrune' q l t) /= []     --wraps around the next function.

shouldPrune' :: QPrefix -> [FAE] -> FAE -> [String]  --returns list of forbidden variables 
shouldPrune' q l t  = let frees = freeVars t         --free in "t" that should be pruned.
                          forbs = [x | x <- frees, x `elem` forb q]
                          frees'= concatMap freeVars l
                      in forbs \\ frees'


\end{code}

The arguments expected by \emph{shouldPrune'} are: the quantifier prefix -- from which the forbidden variables are drawn, a list of operands from the left-hand-side of the (flex-rigid) equation, and the term on the right-hand-side. The function returns the forbidden variables that occur loose on the right-hand-side but not on the left. Pruning will act to remove this disparity, but this might not always be possible. The function described next accepts a term to prune and a list of externally-bound variables and, if pruning is possible, will return a substitution that will eliminate the disparity in loose forbidden variables. It also returns the next fresh variable name to be used by the rest of the unification process. The most interesting clause in this definition concerns redexes; the other cases serve to propagate the search for places where to apply pruning. Finally, function \emph{propagPrune} propagates the search for subterms to prune through the operand-list.

\index{\emph{canPrune}}
\index{\emph{propagPrune}}

\begin{code}
canPrune :: QTerm -> [String] -> Maybe (Subst, String)
--look for a applicative term having shape X w1s z w2s
canPrune (q, Abs vs e) bs = canPrune (q,e) (vs ++ bs)
canPrune (q, App (Var v) l) bs = 
    if v `elem` flex q 
    then --most important clause
        let isFreeForbIn _ []     = Nothing
            isFreeForbIn pre ((Var z):zs) = 
                if z `elem` forb q && not (z `elem` bs) --``z'' is forbidden and not bound.
                then Just (reverse pre,(Var z),zs)
                else isFreeForbIn ((Var z):pre) zs
            --now search for an unbound forbidden variable in arguments to flexible ``v''
            w1s_z_w2s = isFreeForbIn [] l 
        in case w1s_z_w2s of
             Nothing         -> Nothing
             Just(w1s,z,w2s) -> 
                 let v' = freshFrom (flex q) v --generate fresh variable in ``q'' from ``v''
                     sub= Abs (popVarsFAE (w1s++(z:w2s))) (App (Var v') (w1s ++ w2s))
                 in Just ([(v,toCommonNotation sub)],v') --build part of pruning step; this is 
                                                         --the productive part of this function, 
                                                         --the rest is mostly searching.
    else propagPrune q l bs --propagate search to l
canPrune (q, App e1 e2s) bs = 
    let p1 = canPrune (q, e1) bs   --propagate the search
        p2 = propagPrune q e2s bs 
    in case p1 of
         Nothing -> p2
         ans     -> ans
canPrune (q, Var v) bs = Nothing --there's no flex variable at head

\end{code}

\vspace*{1mm}

\begin{code}

propagPrune q [] bs = Nothing --propagate the search for stuff to prune
propagPrune q (x:xs) bs = 
    case (canPrune (q,x) bs) of
      Nothing -> propagPrune q xs bs
      p       -> p
\end{code}



\subsection{Auxiliary functions}

The precise details of the following definitions are omitted since their implementations are straightforward.
In the interest of readability the notation used in this report conceals the constructors used in \emph{Exp} and \emph{FAE} (see \S\ref{sec:Representation}) for the formation of variables. Notwithstanding the absence of explicit markings, a distinction must be drawn between variables as terms and their names. Taking $xs$ to range over lists of variables, $\ulcorner xs \urcorner$ will denote the list of names associated with each variable in $xs$ -- that is, it acts elementwise on $xs$ to project a variable's name. Taking $ns$ to range over lists of variable names, then $\ulcorner\!\ulcorner ns \urcorner\!\urcorner$ denotes the list of variables bearing those names. The symbol $\subseteq_\Lambda$ will be used to denote the subterm relation, and the function $\sharp_{ns} n$ produces the variable name closest to $n$ which is fresh relative to the elements in $ns$. Finally, $\overline{\rho}^\mathrm{fun}$ denotes the function produced from the graph $\rho$.

\index{$\ulcorner xs \urcorner$}
\index{$\ulcorner\!\ulcorner ns \urcorner\!\urcorner$}
\index{$\subseteq_\Lambda$}
\index{$\sharp_{ns} n$}
\index{$\overline{\rho}^\mathrm{fun}$}


\ignore{

\begin{code}

popVarsFAE :: [FAE] -> [String]    --from [Var x] to [x]
popVarsFAE [] = []
popVarsFAE ((Var x):es) = x : (popVarsFAE es)

depopVarsFAE :: [String] -> [FAE]
depopVarsFAE xs = map (\x->Var x) xs

popVarsExp :: [Exp] -> [String]    --from [Var' x] to [x]
popVarsExp xs = map (\(Var' x)->x) xs

depopVarsExp :: [String] -> [Exp]
depopVarsExp xs = map (\x->Var' x) xs


fnise :: Subst -> FAE -> FAE  -- functionalise a substitution operation. switch from FAE to Exp, then back
fnise [] x = x    --empty substitution behaves as identity function
fnise ((v,e):es) x = fnise es (toFunctorArguments ((doSubst v e) (toCommonNotation x)))


subtermOf :: FAE -> FAE -> Bool
subtermOf r s = if r == s
                then True
                else case s of
                       Abs _ t   -> subtermOf r t
                       App t1 t2 -> (subtermOf r t1) || (foldr (||) False (map (subtermOf r) t2))
                       _           -> False


rawEtaEx t = let y = freshFrom (boundVars t' ++ freeVars t') "a"   --eta-expand an expression
             in Abs' y (App' t (Var' y))
             where t' = toFunctorArguments t

freshFrom s f = genTrampoline clip (fresh_to) f --picks out the smallest variable name that's fresh relative to the names in list "s"
                where clip = --trim off anything except the first element where it's fresh for the list of names
                             \l-> head $ filter (\x-> not (x `elem` s)) l


\end{code}

}




\section{Main function} \label{sec:QUnif:main}
Function \emph{unify} combines the behaviour of earlier definitions and iterates the unification algorithm until it yields a solution or fails to find one.

\index{\textbf{\emph{unify}}}

\begin{code}
unify :: UProblem -> Maybe (QPrefix, Subst)
unify p@(q,e) = 
    case (unify' (normalise p)) of   --note: ``normalise'' compacts its result.
      Nothing       -> Nothing
      Just (p',rho) -> 
          if p == p'
          then Just (fst p', rho)
          else case (unify p') of
                 Nothing   -> Nothing
                 Just (q',phi') -> Just (q',restrict (flex q) (rho `o` phi'))

\end{code}


\section{Examples} \label{sec:punif:examples}

Some of the following examples are drawn from the article by \cite{nipkow1993fuh}.
We will start with elementary tests and proceed to test more of the functionality implemented above.

\begin{code}
qpref = foldl (addFlex) empty ["Y","X"]
test1 = unify (qpref,[(Var "X",Var "Y")])
test1' = unify (qpref,[(App (Var "X") [],App (Var "Y") [])])
test1'' = unify (qpref,[(Abs [] (Var "X"),Abs [] (Var "Y"))])
\end{code}
The evaluation of \emph{test1}, \emph{test1'}, and \emph{test1''} yields the same value:
\begin{spec}
Just (
Signatures      : (none)
Flexibles       : X'
Forbiddens      : (none)
,[("X",X'),("Y",X')])
\end{spec}

\noindent The pattern-checking function is tested next.

\begin{code}

p1 = (q,(Abs ["x"] (App (Var "F") [(Abs ["z"] (App (Var "x") [(Var "z")]))])))
     where q = addFlex empty "F"

p1short = (q,(Abs ["x"] (App (Var "F") [(Var "x")])))
          where q = addFlex empty "F"

np4 = (q,(Abs ["x"] (App (Var "G") [(Var "H")])))     --not a pattern
     where q = (foldl) addFlex empty ["G","H"]

p3 = (q,(Abs ["x"] (App (Var "c") [(Var "x")])))    
     where q = addSign empty "c"

np5 = (q,(Abs ["x"] (App (Var "c") [(Var "Z")])))    --not a pattern
      where q = addSign empty "c"

p4 = (q,(Abs ["x","y"] (App (Var "F") [(Var "x"),(Var "y")])))
     where q = addFlex empty "F"

np1 = (q,(App (Var "F") [(Var "c")]))
      where q = addSign (addFlex empty "F") "c"

np2 = (q,(Abs ["x"] (App (Var "F") [(Var "x"),(Var "x")])))
      where q = addFlex empty "F"

np3 = (q,(Abs ["x"] (App (Var "F") [(App (Var "F") [(Var "x")])])))
      where q = addFlex empty "F"

\end{code}

\ignore{
\begin{code}

tests = [isPattern $ compact p1,isPattern $ compact p1short,not $ isPattern $ compact np4,isPattern $ compact p3,not$ isPattern $ compact np5,isPattern $ compact p4,not$ isPattern $ compact np1,not$ isPattern $ compact np2,not$ isPattern $ compact np3]
test t = foldr (&&) True t

\end{code}
}

\noindent The previous definitions whose names do not begin in \emph{np} are patterns.
More interesting behaviour is tested next. Definition \emph{pr1} is not a pattern unification problem, and as 
a result the algorithm misbehaves since it was not presented with valid input.

\begin{code}

pr1 = (q,[(snd p1, snd np4)])               --this is NOT a pattern unification problem
      where q = foldl (addFlex) empty ["F","G","H"]

\end{code}

\noindent The next attempts are all successful and test different parts of the algorithm.

\begin{code}

pr2 = compact ((q,[(Abs ["x","y"] (App (Var "F") [Var "x"]), Abs ["x","y"] (App (Var "c") [App (Var "G") [Var "y", Var "x"]]))])::UProblem)        --flex-rigid
      where q = addSign (foldl (addFlex) empty ["F","G"]) "c"

\end{code}

\begin{spec}
unify pr2
~>Just (
Signatures      : c
Flexibles       : G' and F
Forbiddens      : a''' and a'
,[("G",Abs' "a'''" (Abs' "a'"( App' (Var' "G'") (Var' "a'"))))])
\end{spec}

\vspace*{3mm}

\begin{code}

pr3 = compact ((q,[(Abs ["x","y"] (App (Var "F") [Var "x"]), Abs ["x","y"] (App (Var "G") [Var "y", Var "x"]))])::UProblem)      --flex-flex
      where q = foldl (addFlex) empty ["F","G"]

\end{code}

\begin{spec}
unify pr3
~>Just (
Signatures      : (none)
Flexibles       : F'
Forbiddens      : a''' and a'
,[("F",Abs' "a'" (App' (Var' "F'") (Var' "a'"))),("G",Abs' "a'''"(Abs' "a'"(App' (Var' "F'") (Var' "a'"))))])
\end{spec}

\vspace*{3mm}

\begin{code}
pr4 = compact (q,[(Var "x",App (Var "X") [Var "x"])])::UProblem
      where q = addForb (addFlex empty "X") "x"

\end{code}

\begin{spec}
unify pr4
~>Just (
Signatures      : (none)
Flexibles       : (none)
Forbiddens      : x
,[("X",Abs' "x" (Var' "x"))])
\end{spec}


\addcontentsline{toc}{section}{Notes}
\section*{Notes}

In \S\ref{sec:patterns} the combinator \emph{foldR} was used; curiously an equivalent of this general combinator could not be found in the standard libraries.


