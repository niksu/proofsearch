#!/bin/sh

lhs2TeX --poly Main.lhs > psearch.tex;
bibtex psearch;
latex psearch.tex;
dvips psearch.dvi;
ps2pdf psearch.ps;
