# Proof search in Haskell
This is a Haskell implementation of Helmut Schwichtenberg's proof search algorithm.
Implementations of the algorithm's dependencies are also included:
* (Wand's) type inference algorithm
* Normalisation by Evaluation
* Pattern Unification

The implementation is done in [literate style](https://en.wikipedia.org/wiki/Literate_programming) -- the same files serve both as source code to a program compiler, and type-setting code to a text processor.

Before reading the source code, you might find it useful to first read through the [project report](https://www.nik.network/cucl/files/psearch.pdf).

# Acknowledgements
Helmut Schwichtenberg, Andreas Abel, and the MATHLOGAPS programme.

# License
[ISC](https://en.wikipedia.org/wiki/ISC_license)

# Author
Nik Sultana
