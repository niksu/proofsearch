%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\ignore{

>module ProverData where
>import QPrefix
>import Subst
>import Types
>import Terms
>import NbE
>import Expressions
>import QUnification
>import General

>import Data.List

}


\chapter{Proof system}

An automatic theorem prover can now be built by combining the components from earlier chapters. 

\section{Supporting definitions}

The language of formulas will be described next and shown to be term-like. As in the previous chapter, formulas will then be paired with a $\forall^*\exists^*\forall^*$ quantifier prefix to to yield \emph{Q-formulas}. The theorem prover works over a fragment of Q-formulas formed by \emph{clause} and \emph{goal} Q-formulas; this fragment is characterised in \S\ref{sec:PD:synCheck}.

\index{\emph{Formula}}
\index{\emph{Formula}!$\longrightarrow$}
\index{\emph{Formula}!$\forall$}
\index{\emph{Formula}!$P \langle \mathrm{FAE} \rangle$}


\begin{code}

data Formula 
    = Formula :->: Formula    --implication
    | All [String] Formula    --univ.quantification
    | Pred String [FAE]       --predicate

\end{code}

\ignore{

\begin{code}
    deriving Eq

infixr 5 :->:

\end{code}
}

\vspace*{1mm}

\index{\emph{Term}!\emph{Formula}}

\begin{code}

instance Term Formula where
  freeVars (Pred _ f)   = concatMap freeVars f
  freeVars (All xs f)   = (freeVars f) \\ xs
  freeVars (f1 :->: f2) = (freeVars f1) ++ (freeVars f2)

  boundVars (Pred _ f)  = concatMap boundVars f
  boundVars (All xs f)  = (boundVars f) ++ xs
  boundVars (f1 :->: f2)= (boundVars f1) ++ (boundVars f2)

  compact (Pred n f)    = Pred n (map (compact) f)
  compact (All xs f)    = All xs (compact f)
  compact (f1 :->: f2)  = (compact f1) :->: (compact f2)

  compact1 (Pred n f)    = Pred n (map (compact1) f)
  compact1 (All xs f)    = All xs (compact1 f)
  compact1 (f1 :->: f2)  = (compact1 f1) :->: (compact1 f2)

  normalise (Pred n f)  = Pred n (map (normalise) f)
  normalise (All xs f)  = All xs (normalise f)
  normalise (f1 :->: f2)= (normalise f1) :->: (normalise f2)

\end{code}

\index{\emph{QFormula}}

\begin{code}

type QFormula = (QPrefix, Formula)

\end{code}

The theorem prover will operate on sequents, which will be defined next. The antecedent formulas in a sequent are accompanied by a name and a number: the name serves to identify the assumption variable associated with the assumed formula, and the number is the multiplicity of the assumption -- the maximum number of times the assumption may be used -- and serves to control the complexity of resulting proofs.
The type of sequents are instantiated in the class of terms as shown below.

\index{\emph{Sequent}}
\index{\emph{Sequent}!$\Longrightarrow$}


\begin{code}

data Sequent = [(String,Formula,Int)] :- Formula
\end{code}
\ignore{
\begin{code}
               deriving Eq
\end{code}
}

\index{\emph{Term}!\emph{Sequent}}

\begin{code}

instance Term Sequent where
  freeVars (p :- f)    = let pForms = map (\(_,y,_)->y) p
                         in  nub ((concatMap freeVars pForms) ++ freeVars f)
  boundVars (p :- f)   = let pForms = map (\(_,y,_)->y) p
                         in  nub ((concatMap boundVars pForms) ++ boundVars f)

  compact (p :- f)     = (mapTriple2 (compact) p) :- (compact f)

  compact1 (p :- f)     = (mapTriple2 (compact1) p) :- (compact1 f)

  normalise (p :- f)   = (mapTriple2 (normalise) p) :- (normalise f)


\end{code}

\noindent If a formula in this fragment is provable, the theorem prover will return a proof witnessing this fact as a term. 

\vspace*{1mm}

\index{\emph{Proof}}

\begin{code}

type Proof = Exp

\end{code}



\ignore{

\begin{code}

instance Show Sequent where
 show (p :- f) = showAnte p ++ "\n:-\t  " ++ (show f)
                 where showAnte [] = ""
                       showAnte ((x,y,z):xs) = "\n"++ x ++ "\t: " ++ (show y) ++ (showAnte xs)

instance Show Formula where
 show (Pred n xs) = case xs of
                      [] -> n
                      _  -> n ++ " " ++ (showL xs)
                            where showL [] = ""
                                  showL (x:xs) = case xs of
                                                   [] -> (show x)
                                                   _  -> (show x) ++ " " ++ (showL xs)

 show (f1 :->: f2) = "(" ++ (show f1) ++ " -> " ++ (show f2) ++ ")"
 show (All xs f) = "(all " ++ (commaList xs) ++ ". " ++ (show f) ++ ")"
                   where commaList [] = ""
                         commaList (x:xs) = case xs of
                                              [] -> x
                                              _  -> x ++ "," ++ (commaList xs)



\end{code}

}

\subsection{Lifting combinators}

In the previous chapter, \emph{FAE} was favoured over \emph{Exp} as a representation for $\lambda$-terms. In this chapter both will be used, albeit in different scopes: \emph{Exp} is used to represent proofs, and \emph{FAE} is more wieldy to represent arguments to predicates. The function \emph{liftExpForm} lifts functions over terms -- in \emph{Exp} notation -- to be propagated to terms within formulas. 
This is used, for example, to lift substitutions on terms to operate on terms within formulas.
Similarly, \emph{liftFrmtoSeq} lifts functions defined over formulas to operate on sequents.

\index{\emph{liftExpForm}}
\index{\emph{liftFrmtoSeq}}

\ignore{

\begin{code}

liftExpForm :: (Exp -> Exp) -> (Formula -> Formula)
liftExpForm f (Pred n l) = Pred n (map (toFunctorArguments . f . toCommonNotation) l)
liftExpForm f (f1 :->: f2) = (liftExpForm f f1) :->: (liftExpForm f f2)
liftExpForm f (All xs f1) = All xs' (liftExpForm f f1)
                             where xs' = popVarsExp (map f (depopVarsExp xs))

liftFrmtoSeq :: (Formula -> Formula) -> (Sequent -> Sequent)
liftFrmtoSeq fn (p :- f) = (mapTriple2 fn p) :- (fn f)

\end{code}

}


\subsection{Syntactic checks} \label{sec:PD:synCheck}

The definitions in this section characterise clause and goal Q-formulas, and Q-sequents. An informal description will be given in place of precise definitions here, which are adapted from \cite{schwicht-psml}.
\begin{itemize}
\item If $\vec{r}$ are Q-terms then $P\vec{r}$ is both a Q-clause and Q-goal.
\item If $D$ is a Q-clause and $G$ a Q-goal, then $D \longrightarrow G$ is a Q-goal.
\item If $G$ is a Q-goal and $D$ a Q-clause, then $G \longrightarrow D$ is a Q-clause.
\item If $G$ is a Q$^+_\forall x$-goal, then $\forall x G$ is a Q-goal.
\item If $D\{y \mapsto Y \bullet Q_\forall\}$ is a Q$^+_\exists Y$-clause then $\forall y D$ is a Q-goal.
\end{itemize}


\ignore{

\begin{code}
clauseFormula :: QFormula -> Bool
clauseFormula (q, Pred n f) = foldr (&&) True $ map (\x-> isPattern (q,x)) f
clauseFormula (q, f1 :->: f2) = (goalFormula (q,f1)) && (clauseFormula (q,f2))
clauseFormula (q, All xs f) = 
                              let freshBatch q s 0 = (q,[])  --produce a fresh batch of variables, adding them to prefix as flexible.
                                  freshBatch q s n = let 
                                                         y' = freshFrom (flex q) s
                                                         q' = addFlex q y'
                                                         (q'',rest) = (freshBatch q' y' (n-1))
                                                     in (q'', (y':rest))
                                  (q',ys) = freshBatch q "Y" (length xs)
                                  forbiddens = forb q --doesn't matter if use "q" or "q'" here, since we're projecting the forbiddens
                                  subs = [(x,toCommonNotation $ App (Var y) $ map (\x-> Var x) $ forbiddens) |x <- xs, y <- ys] 
                                  subs'= map (uncurry doSubst) subs
                                  loop [] x     = x
                                  loop (f:fs) x = loop fs (f x)
                                  f' = loop (map liftExpForm subs') f   --the map changes functions on terms to ones on formulas.
                              in clauseFormula (q', f')


goalFormula :: QFormula -> Bool
goalFormula (q, Pred n f) = clauseFormula (q, Pred n f)
goalFormula (q, f1 :->: f2) = (clauseFormula (q,f1)) && (goalFormula (q,f2))
goalFormula (q, All xs f) = goalFormula (q', f)
                            where q' = foldl addForb q xs
\end{code}

\vspace*{1mm}

\begin{code}

type QSequent = (QPrefix, Sequent)

isSequent :: QSequent -> Bool
isSequent (q, ant :- con) = let 
                                ant' = map (\(_,x,_)-> (q,x)) ant
                                con' = (q,con)
                                goal = goalFormula con'
                                clauses = foldr (&&) True $ map clauseFormula ant'
                            in clauses && goal


\end{code}

}

\index{\emph{QSequent}}

