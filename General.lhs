%Copyright 2008 Nik Sultana
%
%Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
%
%THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

\chapter{Supporting functions} \label{sec:General}
\ignore{

>module General where

}

This section collects general functions used in the implementation.
The function \emph{mapp} accepts two lists of values and builds a function that maps between the two coordinate-wise.

\vspace*{1mm}

\index{\emph{mapp}}

>mapp :: Eq a => [a] -> [b] -> a -> b
>mapp [] _ = mapp [] []
>mapp _ [] = mapp [] []
>mapp (x:xs) (y:ys) = 
>                     \z-> if z == x
>                          then y
>                          else mapp xs ys z

\vspace*{1mm}

\noindent The function \emph{countUp} builds a list of $n$ numbered elements.

\vspace*{1mm}

\index{\emph{countUp}}

>countUp :: Integer -> [Integer]
>countUp 0 = []
>countUp 1 = [0]
>countUp n = (n':(countUp n'))
>            where n' = n-1

\vspace*{1mm}

\noindent Function \emph{swap} is used to reorient unification problems.

\vspace*{1mm}

\index{\emph{swap}}

>swap :: (a,b) -> (b,a)
>swap (x,y) = (y,x)

\vspace*{1mm}

\noindent Combinator \emph{foldR} is a more general form of \emph{foldr}, since the step function is passed the rest of the list.

\vspace*{1mm}

\index{\emph{foldR}}

>foldR :: (a -> [a] -> b -> b) -> b -> [a] -> b
>foldR _ b [] = b
>foldR f b (x:xs) = f x xs (foldR f b xs)

\vspace*{1mm}

\noindent Boolean function \emph{distinct} checks that the elements of a list are pairwise distinct.

\vspace*{1mm}

\index{\emph{distinct}}

>distinct l = foldR (\x-> \xs-> \next-> (x `notElem` xs) && next) True l

\newpage

\noindent The following function encodes the standard definition of restriction.

\vspace*{1mm}

\index{$\restriction$}

>restrict [] _          = []
>restrict _ []          = []
>restrict vs ((v,e):es) = 
>                         if v `elem` vs
>                         then ((v,e):(restrict vs es)) --keep
>                         else (restrict vs es)         --drop

\vspace*{1mm}

\noindent The trampoline function \emph{genTrampoline} is used to manage iteration of functions.

\vspace*{1mm}

\index{\emph{genTrampoline}}

>genTrampoline p f x = p $ iterate f x 

\vspace*{1mm}

\noindent Pointwise intersection of two lists is defined next.

\vspace*{1mm}

\index{$\Cap$}

>[] `intersectPT`_ = []
>_ `intersectPT`[] = []
>(x:xs) `intersectPT` (y:ys) = 
>                              if x == y
>                              then (x:(intersectPT xs ys))
>                              else (intersectPT xs ys)

\vspace*{1mm}

\noindent The next definition adapts the \emph{map} function to a list of pairs of the same type.

\vspace*{1mm}

\index{\emph{mapPair}}

>mapPair f l = zip l1 l2
>              where l' = unzip l
>                    l1 = map f (fst l')
>                    l2 = map f (snd l')

\vspace*{1mm}

\noindent The following definition abbreviates mapping a function to the second component in a triple.

\vspace*{1mm}

\index{\emph{mapTriple2}}

>mapTriple2 f l = map (\(x,y,z) -> (x,f y, z)) l

\vspace*{1mm}

\noindent The \emph{replace} function over lists has the obvious definition.

\vspace*{1mm}

\index{\emph{replace}}

>replace :: Eq a => a -> a -> [a] -> [a]
>replace x y [] = []
>replace x y (z:zs) = 
>                     let rest = replace x y zs
>                     in if x == z
>                        then (y:rest)
>                        else (z:rest)

\ignore{

>andList [] = "(none)"
>andList [x] = x
>andList (x:xs) = 
>    case xs of
>            [] -> x
>            _  -> x ++ " and " ++ (andList xs)

}


